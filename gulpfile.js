var gulp = require('gulp');
var browserSync = require('browser-sync');
var packageJson = require('./package.json');
var usemin = require('gulp-usemin');
var wrap = require('gulp-wrap');
var minifyCss = require('gulp-minify-css');
var minifyJs = require('gulp-uglify');
var concat = require('gulp-concat');
var minifyHTML = require('gulp-minify-html');
var clean = require('gulp-clean');
var rename = require('gulp-rename');
var less = require('gulp-less');
var path = require('path');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');

var runSeq = require('run-sequence');


var paths = {
    styles: './styles/**/*.*',
    images: './images/**/*.*',
    templates: './modules/**/*.html',
    index: 'index.html'
};


function less() {
    return gulp.src('./styles/**/*.less')
        .pipe(less())
        .pipe(gulp.dest('./styles/css'));
}

function cleanFiles() {
    return gulp.src('dist').pipe(clean({
        force: true,
        allowEmpty: true
    }));
}

function copyfonts() {
    return gulp.src([
            './bower_components/font-awesome/fonts/*.*',
            './bower_components/bootstrap/fonts/*.*'
        ])
        .pipe(gulp.dest('dist/fonts'));
}

function copyimages() {
    return gulp.src(paths.images)
        .pipe(gulp.dest('dist/images'));
}

function minify() {
    return gulp.src(paths.index)
        .pipe(usemin({
            js: ['concat'],
            js1: [minifyJs().on('error', function(err) {
                console.log('error: ', err)
            }), 'concat'],
            css: [minifyCss({
                keepSpecialComments: 0
            }).on('error', function(err) {
                console.log('error: ', err)
            }), 'concat']
        }))
        .pipe(gulp.dest('dist'));
}

function copytemplates() {
    return gulp.src(paths.templates)
        .pipe(gulp.dest('dist/modules'));
}

function lint() {
    return gulp.src([
            'scripts/**/*.js',
            'modules/**/*.js'
        ])
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
}

function reloadCss() {
    gulp.watch(['modules/**/*.*'])
        .on('change', browserSync.reload);
}

function reloadJs() {
    gulp.watch(['gulpfile.js', 'scripts/**/*.js', 'modules/**/*.js'])
        .on('change', browserSync.reload);
}

function reloadTemplates() {
    gulp.watch(['index.html', 'modules/**/*.html'])
        .on('change', browserSync.reload);
}

function watch() {
    browserSync.init({
        notify: false,
        port: 9005,
        server: "./",
        ui: {
            port: 24680
        }
    });

    gulp.watch('styles/**/*.*', reloadCss);
    gulp.watch(['gulpfile.js', 'scripts/**/*.js', 'modules/**/*.js'], reloadJs);
    gulp.watch(['index.html', 'modules/**/*.html'], reloadTemplates);
}

function serveDist() {
    browserSync.init({
        notify: false,
        port: 9013,
        server: "./dist",
        routes: {
            "bower_components": "../bower_components"
        },
        ui: {
            port: 24681
        }
    });
}

function build() {
    return runSeq('lint', 'clean', 'minify', 'copytemplates', 'copyfonts', 'copyimages');
}

function dist() {
    return runSeq('lint', 'minify', 'copytemplates', 'copyfonts', 'copyimages');
}

gulp.task('less', less);
gulp.task('clean', cleanFiles);
gulp.task('copyfonts', copyfonts);
gulp.task('copyimages', copyimages);
gulp.task('minify', minify);
gulp.task('copytemplates', copytemplates);
gulp.task('lint', lint);
gulp.task('reload-css', reloadCss);
gulp.task('reload-js', gulp.parallel(lint, reloadJs));
gulp.task('reload-templates', reloadTemplates);
gulp.task('watch', watch);
gulp.task('serve', gulp.parallel(lint, watch));
gulp.task('servedist', serveDist);
gulp.task('build', gulp.series(lint, cleanFiles, minify, copytemplates, copyfonts, copyimages));
gulp.task('dist', gulp.series(lint, minify, copytemplates, copyfonts, copyimages));