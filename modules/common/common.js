(function() {
    'use strict';

    angular.module('starter')
        .factory('commonFctry', commonFctry);

    commonFctry.$inject = ['Restangular'];

    function commonFctry(Restangular) {
        return {
            getAllCountries: function() {
                return Restangular.all('country').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            }
        };
    }

})();