(function() {
    'use strict';

    angular.module('starter')
        .controller('buyerCtrl', buyerCtrl);

    buyerCtrl.$inject = ['$scope', '$uibModal', 'buyersFctory', 'NgTableParams', '$filter'];

    function buyerCtrl($scope, $uibModal, buyersFctory, NgTableParams, $filter) {
        $scope.buyers = [];
        $scope.dataSearch = '';

        $scope.tableParams = new NgTableParams({
            page: 1, // show first page
            count: 10, // count per page
        }, {
            getData: function(params) {
                return buyersFctory.getAllBuyerUsers().then(function(data) {
                    if (data && data.success) {
                        var buyers = data.result;
                        if ($scope.dataSearch) {
                            $scope.buyers = $filter('filter')(buyers.result, $scope.dataSearch);
                        } else {
                            $scope.buyers = buyers.result;
                        }

                        console.log('$scope.buyers: ',$scope.buyers);

                        params.total($scope.buyers.length);
                        return ($scope.buyers.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    }
                });
            }
        });

        $scope.refresh = function() {
            $scope.tableParams.reload();
        };

        $scope.addSeller = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/seller/seller.modal.html',
                controller: 'buyerModalCtrl',
                size: 'md',
            });

            modalInstance.result.then(function() {

            }, function() {
                console.info('Modal dismissed at: ' + new Date());
            });
        };
    }
})();