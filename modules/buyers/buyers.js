(function() {
    'use strict';

    angular.module('starter')
        .config(config)
        .factory('buyersFctory', buyersFctory);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('app.buyers', {
                url: '/buyers',
                templateUrl: 'modules/buyers/buyers.html',
                data: {
                    pageTitle: 'Buyers / Users'
                },
                controller: 'buyerCtrl',
            });

    }

    buyersFctory.$inject = ['Restangular'];

    function buyersFctory(Restangular){
         return {
            getAllBuyerUsers: function() {
                return Restangular.all('buyers').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            }
        };
    }
})();