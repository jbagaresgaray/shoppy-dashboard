(function() {
    'use strict';

    angular.module('starter')
        .controller('buyerModalCtrl', buyerModalCtrl);

    buyerModalCtrl.$inject = ['$scope', '$uibModalInstance'];

    function buyerModalCtrl($scope, $uibModalInstance) {

        $scope.save = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();