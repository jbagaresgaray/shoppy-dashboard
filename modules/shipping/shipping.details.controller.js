(function() {
    'use strict';

    angular.module('starter')
        .controller('shippingTrackingTrackCtrl', shippingTrackingTrackCtrl);

    shippingTrackingTrackCtrl.$inject = ['_', '$scope', '$state', '$stateParams', 'shippingTrackingFctry'];

    function shippingTrackingTrackCtrl(_, $scope, $state, $stateParams, shippingTrackingFctry) {
        console.log('shippingTrackingTrackCtrl: ', $stateParams);
        $scope.tracking = {};
        $scope.tracking.checkpoints = [];

        if ($stateParams.orderNum) {
            shippingTrackingFctry.getOrderDetailsByOrderNum($stateParams.orderNum).then(function(data) {
                if (data && data.success) {
                    $scope.tracking = data.result;
                    $scope.tracking.checkpoints = [];

                    $scope.tracking.checkpoints.push({
                        'slug': $scope.tracking.courier_slug,
                        'city': $scope.tracking.orderShipCity,
                        'created_at': $scope.tracking.orderDateTime,
                        'location': $scope.tracking.orderShipAddress,
                        'country_name': null,
                        'message': 'Order details sent to seller',
                        'country_iso3': $scope.tracking.orderShipCountry,
                        'tag': 'InfoReceived',
                        'subtag': 'InfoReceived_002',
                        'subtag_message': 'Info Received',
                        'checkpoint_time': new Date($scope.tracking.orderDateTime),
                        'coordinates': [],
                        'state': $scope.tracking.orderShipState,
                        'zip': $scope.tracking.orderShipZipCode,
                    }, {
                        'slug': $scope.tracking.courier_slug,
                        'city': $scope.tracking.orderShipCity,
                        'created_at': $scope.tracking.orderDateTime,
                        'location': $scope.tracking.orderShipAddress,
                        'country_name': null,
                        'message': 'Order Created',
                        'country_iso3': $scope.tracking.orderShipCountry,
                        'tag': 'InfoReceived',
                        'subtag': 'InfoReceived_001',
                        'subtag_message': 'Info Received',
                        'checkpoint_time': new Date($scope.tracking.orderDateTime),
                        'coordinates': [],
                        'state': $scope.tracking.orderShipState,
                        'zip': $scope.tracking.orderShipZipCode,
                    });
                }
            }, function(error) {
                console.log('error: ', error);
            });
        }
    }
})();

(function() {
    'use strict';

    angular.module('starter')
        .controller('shippingTrackingDetailsCtrl', shippingTrackingDetailsCtrl);

    shippingTrackingDetailsCtrl.$inject = ['_', '$scope', '$state', '$stateParams', 'shippingTrackingFctry', '$uibModal'];

    function shippingTrackingDetailsCtrl(_, $scope, $state, $stateParams, shippingTrackingFctry, $uibModal) {
        console.log('shippingTrackingDetailsCtrl: ');

        $scope.tracking = {};
        $scope.tracking.checkpoints = [];

        $scope.refresh = function() {
            if ($stateParams.orderNum) {
                shippingTrackingFctry.getOrderTrackingByOrderNum($stateParams.orderNum).then(function(data) {
                    if (data && data.success) {
                        $scope.tracking = data.result;

                        $scope.tracking.checkpoints.push({
                            'slug': $scope.tracking.slug,
                            'city': $scope.tracking.pickUpCity,
                            'created_at': $scope.tracking.created_at,
                            'location': $scope.tracking.pickUpLocation,
                            'country_name': null,
                            'message': 'Order details sent to seller',
                            'country_iso3': $scope.tracking.pickUpCountry,
                            'tag': 'InfoReceived',
                            'subtag': 'InfoReceived_002',
                            'subtag_message': 'Info Received',
                            'checkpoint_time': new Date($scope.tracking.created_at),
                            'coordinates': [],
                            'state': $scope.tracking.pickUpState,
                            'zip': $scope.tracking.pickUpZip,
                        }, {
                            'slug': $scope.tracking.slug,
                            'city': $scope.tracking.pickUpCity,
                            'created_at': $scope.tracking.created_at,
                            'location': $scope.tracking.pickUpLocation,
                            'country_name': null,
                            'message': 'Order Created',
                            'country_iso3': $scope.tracking.pickUpCountry,
                            'tag': 'InfoReceived',
                            'subtag': 'InfoReceived_001',
                            'subtag_message': 'Info Received',
                            'checkpoint_time': new Date($scope.tracking.created_at),
                            'coordinates': [],
                            'state': $scope.tracking.pickUpState,
                            'zip': $scope.tracking.pickUpZip,
                        });
                    }
                    console.log('$scope.tracking: ', $scope.tracking);
                }, function(error) {
                    console.log('error: ', error);
                });
            }
        };


        $scope.refresh();

        $scope.updateTracking = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/shipping/shipping.tracking.modal.html',
                controller: 'shippingTrackingDetailsModalCtrl',
                size: 'md',
                resolve: {
                    orderNum: function() {
                        return $scope.tracking.orderNum;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.refresh();
                }
            }, function() {});
        };
    }
})();

(function() {
    'use strict';

    angular.module('starter')
        .controller('shippingTrackingDetailsModalCtrl', shippingTrackingDetailsModalCtrl);

    shippingTrackingDetailsModalCtrl.$inject = ['_', 'async', '$scope', '$uibModalInstance', 'shippingTrackingFctry', 'orderNum', 'toastr', '$timeout'];

    function shippingTrackingDetailsModalCtrl(_, async, $scope, $uibModalInstance, shippingTrackingFctry, orderNum, toastr, $timeout) {
        $scope.tracking = {};
        $scope.content = {};

        $scope.shippingFlags = [];
        $scope.shippingCopy = [];
        $scope.countryArr = [];
        $scope.shippingTags = [];

        $scope.isSaving = false;
        $scope.showSignedBy = false;
        $scope.showDeliveredBy = false;
        $scope.disableFlags = true;


        console.log('orderNum: ', orderNum);

        if (orderNum) {
            async.waterfall([
                function(callback) {
                    shippingTrackingFctry.getTrackingCountry().then(function(data) {
                        if (data && data.success) {
                            $scope.countryArr = data.result;
                        }
                        callback();
                    }, function(error) {
                        console.log('error: ', error);
                        callback();
                    });
                },
                function(callback) {
                    shippingTrackingFctry.getTrackingShippingTags().then(function(data) {
                        if (data && data.success) {
                            $scope.shippingTags = data.result;
                        }
                        callback();
                    }, function(error) {
                        console.log('error: ', error);
                        callback();
                    });
                },
                function(callback) {
                    shippingTrackingFctry.getTrackingCheckingFlags().then(function(data) {
                        if (data && data.success) {
                            $scope.shippingCopy = data.result;
                        }
                        callback();
                    }, function(error) {
                        console.log('error: ', error);
                        callback();
                    });
                },
                function() {
                    shippingTrackingFctry.getOrderTrackingByOrderNum(orderNum).then(function(data) {
                        if (data && data.success) {
                            $scope.tracking = data.result;
                        }
                    }, function(error) {
                        console.log('error: ', error);
                    });
                }
            ]);
        }

        $scope.$watch('content.tags', function(newvalue) {
            console.log('content.tags: ', newvalue);
            if (newvalue) {
                $scope.disableFlags = false;
                $scope.shippingFlags = _.filter($scope.shippingCopy, {
                    'tag': newvalue.tag
                });
            }
        });

        $scope.$watch('content.flags', function(newvalue) {
            console.log('newvalue: ', newvalue);
            if (newvalue) {
                if (newvalue.order_checking_flagsId === 1) {
                    $scope.showSignedBy = true;

                    $scope.content.location = $scope.tracking.pickUpLocation;
                    $scope.content.country_iso3 = $scope.tracking.pickUpCountry;
                    $scope.content.state = $scope.tracking.pickUpState;
                    $scope.content.zip = $scope.tracking.pickUpZip;
                    $scope.content.city = $scope.tracking.pickUpCity;
                } else if (newvalue.order_checking_flagsId === 8) {
                    $scope.showDeliveredBy = true;
                } else {
                    $scope.content.location = '';
                    $scope.content.country_iso3 = '';
                    $scope.content.state = '';
                    $scope.content.zip = '';
                    $scope.content.city = '';

                    $scope.showDeliveredBy = false;
                    $scope.showSignedBy = false;
                }
            }
        });

        $scope.saveEntry = function() {
            $scope.isSaving = true;

            $scope.content.order_trackingId = $scope.tracking.order_trackingId;
            $scope.content.orderNum = $scope.tracking.orderNum;
            $scope.content.orderId = $scope.tracking.orderId;
            $scope.content.created_at = new Date();
            $scope.content.slug = $scope.tracking.slug;

            if ($scope.content.tags) {
                $scope.content.tag = $scope.content.tags.tag;
                $scope.content.subtag_message = $scope.content.tags.status;
            }

            if ($scope.content.flags) {
                if ($scope.content.flags.order_checking_flagsId === 1 && $scope.content.flags.tag === 'InTransit') {
                    $scope.content.isPickUp = 1;

                    $scope.content.location = $scope.tracking.pickUpLocation;
                    $scope.content.country_name = $scope.tracking.pickUpCountry;
                    $scope.content.country_iso3 = $scope.tracking.pickUpCountry;
                    $scope.content.state = $scope.tracking.pickUpState;
                    $scope.content.zip = $scope.tracking.pickUpZip;
                    $scope.content.city = $scope.tracking.pickUpCity;

                    $scope.content.message = $scope.content.flags.flags + ' (' + $scope.content.tracking_number + ')';

                } else if ($scope.content.flags.order_checking_flagsId === 8 && $scope.content.flags.tag === 'OutForDelivery') {
                    $scope.content.message = $scope.content.flags.flags + ' ' + $scope.content.deliveredby;
                    $scope.content.isPickUp = $scope.tracking.isPickUp;
                    $scope.content.tracking_number = $scope.tracking.tracking_number;
                    $scope.content.signed_by = $scope.tracking.signed_by;
                } else {
                    $scope.content.isPickUp = $scope.tracking.isPickUp;
                    $scope.content.tracking_number = $scope.tracking.tracking_number;
                    $scope.content.signed_by = $scope.tracking.signed_by;
                    $scope.content.message = $scope.content.flags.flags;
                }

                if ($scope.content.country) {
                    $scope.content.country_name = $scope.content.country.name;
                    $scope.content.country_iso3 = $scope.content.country.code;
                }
                $scope.content.subtag = $scope.content.flags.tag + '_' + $scope.content.flags.subtag_num;
            } else {
                $scope.content.isPickUp = $scope.tracking.isPickUp;
            }



            async.waterfall([
                function(callback) {
                    console.log('$scope.content: ', $scope.content);
                    shippingTrackingFctry.updateOrderTracking($scope.content.order_trackingId, $scope.content).then(function(data) {
                        if (data && data.success) {
                            toastr.success(data.msg, 'Success');
                            $scope.isSaving = false;
                            $timeout(function() {
                                $uibModalInstance.close('save');
                            }, 300);
                        } else if (!data.success && _.isArray(data.result)) {
                            _.each(data.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                            $scope.isSaving = false;
                        } else {
                            toastr.error(data.msg, 'Error');
                            $scope.isSaving = false;
                        }
                        callback();
                    }, function(error) {
                        console.log('error: ', error);
                        $scope.isSaving = false;
                        callback();
                    });
                },
                function(callback) {
                    if ($scope.content.flags.order_checking_flagsId === 1 && $scope.content.flags.tag === 'InTransit') {
                        shippingTrackingFctry.shippedOrder($scope.tracking.orderNum, {
                            shippedDateTime: new Date(),
                            tracking_number: $scope.content.tracking_number
                        }).then(function(data) {
                            if (data && data.success) {
                                toastr.success(data.msg, 'Success');
                            } else if (!data.success && _.isArray(data.result)) {
                                _.each(data.result, function(msg) {
                                    toastr.warning(msg.msg, 'Warning');
                                });
                            } else {
                                toastr.error(data.msg, 'Error');
                            }
                            callback();
                        }, function(error) {
                            console.log('error: ', error);
                            $scope.isSaving = false;
                            callback();
                        });
                    } else if ($scope.content.flags.order_checking_flagsId === 7 && $scope.content.flags.tag === 'Delivered') {
                        shippingTrackingFctry.orderReceivedReminder($scope.tracking.orderId).then(function(data) {
                            if (data && data.success) {
                                toastr.success(data.msg, 'Success');
                            } else if (!data.success && _.isArray(data.result)) {
                                _.each(data.result, function(msg) {
                                    toastr.warning(msg.msg, 'Warning');
                                });
                            } else {
                                toastr.error(data.msg, 'Error');
                            }
                            callback();
                        }, function(error) {
                            console.log('error: ', error);
                            $scope.isSaving = false;
                            callback();
                        });
                    } else {
                        callback();
                    }
                },
            ]);

        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();