(function() {
    'use strict';

    angular.module('starter')
        .controller('shippingTrackingCtrl', shippingTrackingCtrl);

    shippingTrackingCtrl.$inject = ['_', '$scope', '$state', 'NgTableParams', 'shippingTrackingFctry', '$filter'];

    function shippingTrackingCtrl(_, $scope, $state, NgTableParams, shippingTrackingFctry, $filter) {
        console.log('shippingTrackingCtrl:');

        $scope.trackingArr = [];

        $scope.tableParams = new NgTableParams({
            page: 1, // show first page
            count: 10, // count per page
        }, {
            getData: function(params) {
                return shippingTrackingFctry.getAllOrderTrackings().then(function(data) {
                    console.log('getAllOrderTrackings: ', data);
                    var trackingArr = data.result;

                    if ($scope.dataSearch) {
                        $scope.trackingArr = $filter('filter')(trackingArr, $scope.dataSearch);
                    } else {
                        $scope.trackingArr = trackingArr;
                    }

                    params.total($scope.trackingArr.length);
                    return ($scope.trackingArr.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }, function(error) {
                    console.log('error: ', error);
                });
            }
        });


        $scope.trackOrder = function(order) {
            $state.go('app.shippingtracking_tracking', {
                orderNum: order.orderNum
            });
        };

        $scope.viewOrderDetails = function(order) {
            $state.go('app.shippingtracking_info', {
                orderNum: order.orderNum
            });
        };

        $scope.deleteSelected = function() {

        };
    }
})();