(function() {
    'use strict';

    angular.module('starter')
        .config(config)
        .factory('shippingTrackingFctry', shippingTrackingFctry);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('app.shippingtracking', {
                url: '/shippingtracking',
                templateUrl: 'modules/shipping/shipping.html',
                data: {
                    pageTitle: 'Shipping Tracking'
                },
                controller: 'shippingTrackingCtrl'
            })
            .state('app.shippingtracking_info', {
                url: '/shippingtracking/info/:orderNum',
                templateUrl: 'modules/shipping/shipping.details.html',
                data: {
                    pageTitle: 'Order Tracking'
                },
                controller: 'shippingTrackingDetailsCtrl'
            })
            .state('app.shippingtracking_tracking', {
                url: '/shippingtracking/track/:orderNum',
                templateUrl: 'modules/shipping/shipping.tracking.html',
                data: {
                    pageTitle: 'Order Tracking'
                },
                controller: 'shippingTrackingTrackCtrl'
            });
    }


    shippingTrackingFctry.$inject = ['Restangular'];

    function shippingTrackingFctry(Restangular) {
        return {
            getAllOrderTrackings: function(params) {
                return Restangular.all('trackings').customGET(null, {
                    action: params
                }).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getOrderTrackingByOrderNum: function(orderNum) {
                return Restangular.all('trackings').customGET(orderNum).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getOrderTrackingByTrackingNum: function(slug, tracking_number) {
                return Restangular.all('trackings/' + slug + '/' + tracking_number).customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getOrderDetailsByOrderNum: function(orderNum) {
                return Restangular.all('orders').customGET(orderNum).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getTrackingCheckingFlags: function() {
                return Restangular.all('shipping_flags').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            updateOrderTracking: function(order_trackingId,data){
                return Restangular.all('trackings/' + order_trackingId + '/track').customPUT(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getTrackingCountry: function() {
                return Restangular.all('country').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getTrackingShippingTags: function() {
                return Restangular.all('shipping_tags').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            shippedOrder: function(orderId,data){
                return Restangular.all('orders/' + orderId + '/shipped').customPUT(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            orderReceivedReminder: function(orderId){
                return Restangular.all('orders/' + orderId + '/receive_reminder').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            }
        };
    }
})();