(function() {
    'use strict';

    angular.module('starter')
        .controller('newsLetterCtrl', newsLetterCtrl)
        .controller('composeNewsLetterCtrl', composeNewsLetterCtrl);

    newsLetterCtrl.$inject = ['_', '$scope', '$state', 'toastr', 'ngDialog', 'newsLetterFctry'];

    function newsLetterCtrl(_, $scope, $state, toastr, ngDialog, newsLetterFctry) {

        $scope.newsletters = [];

        newsLetterFctry.getAllNewsletter().then(function(data) {
            if (data && data.success) {
                $scope.newsletters = data.result;
                _.each($scope.newsletters, function(row) {
                    row.tags = row.tags.split(',');
                });
                console.log('$scope.newsletters: ', $scope.newsletters);
            }
        });

        $scope.composeNewsletter = function() {
            $state.go('app.composenews');
        };

        $scope.refresh = function() {
            $scope.newsletters = [];

            newsLetterFctry.getAllNewsletter().then(function(data) {
                if (data && data.success) {
                    $scope.newsletters = data.result;
                    _.each($scope.newsletters, function(row) {
                        row.tags = row.tags.split(',');
                    });
                    console.log('$scope.newsletters: ', $scope.newsletters);
                }
            });
        };

        $scope.deleteNews = function(cmsId) {
            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                newsLetterFctry.deleteNewsletter(cmsId).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.refresh();
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }, function() {

            });
        };
    }


    composeNewsLetterCtrl.$inject = ['_', '$scope', '$state', '$stateParams', 'toastr', 'ngDialog', 'newsLetterFctry', '$timeout'];

    function composeNewsLetterCtrl(_, $scope, $state, $stateParams, toastr, ngDialog, newsLetterFctry, $timeout) {
        $scope.content = {};
        $scope.content.tags = [];
        $scope.notiftype = [];

        newsLetterFctry.getAllNotificationType().then(function(data) {
            if (data && data.success) {
                $scope.notiftype = data.result;
            }
        });


        if ($stateParams && $stateParams.cmsId) {
            newsLetterFctry.getNewsletter($stateParams.cmsId).then(function(data) {
                if (data && data.success) {
                    $scope.content = data.result;
                    if ($scope.content.tags) {
                        $scope.content.tags = $scope.content.tags.split(',');
                    }
                    console.log('$scope.content: ', $scope.content);
                } else if (!data.success && _.isArray(data.result)) {
                    _.each(data.result, function(msg) {
                        toastr.warning(msg.msg, 'Warning');
                    });
                    $scope.isSaving = false;
                } else {
                    toastr.error(data.msg, 'Error');
                    $scope.isSaving = false;
                }
            });
        }

        $scope.loadTags = function(tag) {
            console.log('tag: ', tag);
            newsLetterFctry.getTags(tag).then(function(data) {
                if (data && data.success) {
                    $scope.content.tags = data.result;
                }
            });
        };

        $scope.publish = function() {
            $scope.modal = {
                title: 'Publish Newsletter?',
                message: 'Do you want to publish newsletter?'
            };

            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                newsLetterFctry.publishNewsletter($stateParams.cmsId, 'push').then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.isSaving = false;
                        $timeout(function() {
                            $state.go('app.newsletters');
                        }, 300);
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                        $scope.isSaving = false;
                    } else {
                        toastr.error(data.msg, 'Error');
                        $scope.isSaving = false;
                    }
                });
            }, function() {

            });
        };



        $scope.emailNotify = function(){
            $scope.modal = {
                title: 'Email Newsletter?',
                message: 'Do you want to send newsletter as email?'
            };

            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                newsLetterFctry.emailNewsletter($stateParams.cmsId).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.isSaving = false;
                        $timeout(function() {
                            $state.go('app.newsletters');
                        }, 300);
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                        $scope.isSaving = false;
                    } else {
                        toastr.error(data.msg, 'Error');
                        $scope.isSaving = false;
                    }
                });
            }, function() {

            });
        };


        $scope.pushNotify = function(){
            $scope.modal = {
                title: 'Push Newsletter?',
                message: 'Do you want to send newsletter as push?'
            };

            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                newsLetterFctry.pushNewsletter($stateParams.cmsId).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.isSaving = false;
                        $timeout(function() {
                            $state.go('app.newsletters');
                        }, 300);
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                        $scope.isSaving = false;
                    } else {
                        toastr.error(data.msg, 'Error');
                        $scope.isSaving = false;
                    }
                });
            }, function() {

            });
        };



        $scope.unpublish = function() {
            $scope.modal = {
                title: 'Unpublish Newsletter?',
                message: 'Do you want to unpublish newsletter?'
            };

            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                newsLetterFctry.unpublishNewsletter($stateParams.cmsId).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.isSaving = false;
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                        $scope.isSaving = false;
                    } else {
                        toastr.error(data.msg, 'Error');
                        $scope.isSaving = false;
                    }
                });
            }, function() {

            });
        };

        $scope.save = function() {
            if ($stateParams && $stateParams.cmsId) {
                $scope.modal = {
                    title: 'Update Newsletter?',
                    message: 'Do you want to update newsletter?'
                };

                ngDialog.openConfirm({
                    templateUrl: './modules/dialogs/custom.dialog.html',
                    scope: $scope,
                    className: 'ngdialog-theme-default'
                }).then(function() {
                    newsLetterFctry.updateNewsletter($stateParams.cmsId, $scope.content).then(function(data) {
                        if (data && data.success) {
                            toastr.success(data.msg, 'Success');
                            $scope.isSaving = false;
                            $timeout(function() {
                                $state.go('app.newsletters');
                            }, 300);
                        } else if (!data.success && _.isArray(data.result)) {
                            _.each(data.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                            $scope.isSaving = false;
                        } else {
                            toastr.error(data.msg, 'Error');
                            $scope.isSaving = false;
                        }
                    });
                }, function() {

                });
            } else {
                $scope.modal = {
                    title: 'Save Newsletter?',
                    message: 'Do you want to save newsletter?'
                };

                ngDialog.openConfirm({
                    templateUrl: './modules/dialogs/custom.dialog.html',
                    scope: $scope,
                    className: 'ngdialog-theme-default'
                }).then(function() {
                    newsLetterFctry.createNewsletter($scope.content).then(function(data) {
                        if (data && data.success) {
                            toastr.success(data.msg, 'Success');
                            $scope.isSaving = false;
                            $timeout(function() {
                                $state.go('app.newsletter_detail', {
                                    cmsId: data.result
                                });
                            }, 300);
                        } else if (!data.success && _.isArray(data.result)) {
                            _.each(data.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                            $scope.isSaving = false;
                        } else {
                            toastr.error(data.msg, 'Error');
                            $scope.isSaving = false;
                        }
                    });
                }, function() {

                });
            }
        };
    }


})();