(function() {
    'use strict';

    angular.module('starter')
        .config(config)
        .factory('newsLetterFctry', newsLetterFctry);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('app.newsletters', {
                url: '/newsletter',
                templateUrl: 'modules/newsletter/newsletter.html',
                data: {
                    pageTitle: 'Newsletter'
                },
                controller: 'newsLetterCtrl'
            })
            .state('app.newsletter_detail', {
                url: '/newsletter/:cmsId',
                templateUrl: 'modules/newsletter/article.html',
                data: {
                    pageTitle: 'Articles'
                },
                controller: 'composeNewsLetterCtrl'
            })
            .state('app.composenews', {
                url: '/newsletter/compose',
                templateUrl: 'modules/newsletter/compose.html',
                controller: 'composeNewsLetterCtrl',
                data: {
                    pageTitle: 'Compose Newsletter'
                }
            })
            .state('app.updatenews', {
                url: '/newsletter/:cmsId/update',
                templateUrl: 'modules/newsletter/compose.html',
                controller: 'composeNewsLetterCtrl',
                data: {
                    pageTitle: 'Compose Newsletter'
                }
            });

    }


    newsLetterFctry.$inject = ['Restangular'];

    function newsLetterFctry(Restangular) {
        return {
            getAllNotificationType: function(){
                return Restangular.all('notiftype').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getTags: function(){
                return Restangular.all('tags').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getAllNewsletter: function() {
                return Restangular.all('newsletter').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            createNewsletter: function(data) {
                return Restangular.all('newsletter').customPOST(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getNewsletter: function(cmsId) {
                return Restangular.all('newsletter').customGET(cmsId).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            updateNewsletter: function(cmsId,data){
                return Restangular.all('newsletter/' + cmsId).customPUT(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            deleteNewsletter: function(cmsId){
                return Restangular.all('newsletter').customDELETE(cmsId).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            publishNewsletter: function(cmsId){
                return Restangular.all('newsletter/' + cmsId + '/publish').customPUT().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            unpublishNewsletter: function(cmsId){
                return Restangular.all('newsletter/' + cmsId + '/unpublish').customPUT().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            pushNewsletter: function(cmsId){
                return Restangular.all('newsletter/' + cmsId + '/push').customPOST().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            emailNewsletter: function(cmsId){
                return Restangular.all('newsletter/' + cmsId + '/email').customPOST().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
        };
    }
})();