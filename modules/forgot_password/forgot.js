(function() {
    'use strict';

    angular.module('starter')
        .config(forgotConfig)
        .factory('forgotFctry', forgotFctry);

    forgotConfig.$inject = ['$stateProvider'];

    function forgotConfig($stateProvider) {
        $stateProvider
            .state('forgot_password', {
                url: '/forgot_password',
                templateUrl: 'modules/forgot_password/forgot_password.html',
                data: { pageTitle: 'Forgot Password', specialClass: 'gray-bg' }
            });
    }

    forgotFctry.$inject = ['Restangular'];

    function forgotFctry(Restangular) {
        return {
            forgotPassword: function(data) {
                return Restangular.all('forgotpassword').customPOST(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
        };
    }
})();