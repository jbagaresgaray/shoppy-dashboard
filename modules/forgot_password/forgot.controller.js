(function() {
    'use strict';

    angular.module('starter')
        .controller('forgotCtrl', ['$scope', '$state', 'toastr', 'forgotFctry', '_',
            function AuthCtrl($scope, $state, toastr, forgotFctry, _) {
                $scope.user = {};

                $scope.forgotpass = function() {
                    forgotFctry.forgotPassword($scope.user).then(function(resp) {
                        if (resp.response.success) {
                            toastr.success(resp.msg, 'Success');
                            $scope.user = {};
                        } else if (!resp.success && _.isArray(resp.result)) {
                            _.each(resp.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            toastr.error(resp.msg, 'Error');
                        }
                    });
                };
            }
        ]);
})();