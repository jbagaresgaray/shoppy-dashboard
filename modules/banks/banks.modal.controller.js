    (function() {
        'use strict';

        angular.module('starter')
            .controller('banksModalCtrl', banksModalCtrl);

        banksModalCtrl.$inject = ['_', '$scope', '$uibModalInstance', 'commonFctry', 'banksFctry', 'bankId', 'toastr', '$timeout'];

        function banksModalCtrl(_, $scope, $uibModalInstance, commonFctry, banksFctry, bankId, toastr, $timeout) {
            $scope.countries = [];
            $scope.bank = {};
            $scope.isSaving = false;

            commonFctry.getAllCountries().then(function(data) {
                if (data && data.success) {
                    $scope.countries = data.result;
                }
            });

            if (bankId) {
                banksFctry.getBankById(bankId).then(function(data) {
                    if (data && data.success) {
                        var bank = data.result;
                        if (!_.isEmpty(bank)) {
                            $scope.bank = bank;
                        }
                    }
                });
            }

            $scope.save = function() {
                $scope.isSaving = true;
                if (bankId) {
                    banksFctry.updateBankById(bankId, $scope.bank).then(function(data) {
                        if (data && data.success) {
                            toastr.success(data.msg, 'Success');
                            $scope.isSaving = false;
                            $timeout(function() {
                                $uibModalInstance.close('save');
                            }, 300);
                        } else if (!data.success && _.isArray(data.result)) {
                            $scope.isSaving = false;
                            _.each(data.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            $scope.isSaving = false;
                            toastr.error(data.msg, 'Error');
                        }
                    });
                } else {
                    banksFctry.createBank($scope.bank).then(function(data) {
                        if (data && data.success) {
                            toastr.success(data.msg, 'Success');
                            $scope.isSaving = false;
                            $timeout(function() {
                                $uibModalInstance.close('save');
                            }, 300);
                        } else if (!data.success && _.isArray(data.result)) {
                            $scope.isSaving = false;
                            _.each(data.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            $scope.isSaving = false;
                            toastr.error(data.msg, 'Error');
                        }
                    });
                }
            };

            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
        }
    })();