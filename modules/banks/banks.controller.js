(function() {
    'use strict';

    angular.module('starter')
        .controller('banksCtrl', banksCtrl);

    banksCtrl.$inject = ['$scope', '$uibModal', 'banksFctry', 'NgTableParams', 'ngDialog', '$filter', 'toastr'];

    function banksCtrl($scope, $uibModal, banksFctry, NgTableParams, ngDialog, $filter, toastr) {
        $scope.banks = [];
        $scope.dataSearch = '';

        $scope.tableParams = new NgTableParams({
            page: 1, // show first page
            count: 10, // count per page
        }, {
            getData: function(params) {
                return banksFctry.getAllBanks().then(function(data) {
                    if (data && data.success) {
                        var banks = data.result;

                        if ($scope.dataSearch) {
                            $scope.banks = $filter('filter')(banks, $scope.dataSearch);
                        } else {
                            $scope.banks = banks;
                        }

                        params.total($scope.banks.length);
                        return ($scope.banks.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    }
                });
            }
        });

        $scope.addBank = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/banks/banks.modal.html',
                controller: 'banksModalCtrl',
                size: 'md',
                resolve: {
                    bankId: function() {
                        return null;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.tableParams.reload();
                }
            }, function() {});
        };

        $scope.updateBank = function(item) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/banks/banks.modal.html',
                controller: 'banksModalCtrl',
                size: 'md',
                resolve: {
                    bankId: function() {
                        return item.bankId;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.tableParams.reload();
                }
            }, function() {});
        };

        $scope.deleteBank = function(item) {
            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                banksFctry.deleteBankById(item.bankId).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.tableParams.reload();
                    }
                });
            }, function() {

            });
        };

        $scope.refresh = function() {
            $scope.tableParams.reload();
        };

    }
})();