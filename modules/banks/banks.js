(function() {
    'use strict';

    angular.module('starter')
        .config(config)
        .factory('banksFctry', banksFctry);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('app.banks', {
                url: '/banks',
                templateUrl: 'modules/banks/banks.html',
                data: {
                    pageTitle: 'Banks'
                },
                controller: 'banksCtrl'
            });
    }

    banksFctry.$inject = ['Restangular'];

    function banksFctry(Restangular) {
        return {
            getAllBanks: function() {
                return Restangular.all('banks').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            createBank: function(data) {
                return Restangular.all('banks').customPOST(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getBankById: function(bankId) {
                return Restangular.all('banks').customGET(bankId).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            deleteBankById: function(bankId) {
                return Restangular.all('banks').customDELETE(bankId).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            updateBankById: function(bankId, data) {
                return Restangular.all('banks/' + bankId).customPUT(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            }
        };
    }

})();