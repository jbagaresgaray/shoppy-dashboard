(function() {
	'use strict';

	angular.module('starter')
		.controller('officialShopsCtrl', officialShopsCtrl)
		.controller('officialShopDetailCtrl', officialShopDetailCtrl);

	officialShopsCtrl.$inject = ['$scope', 'NgTableParams', 'officialShopsFctry', '$filter'];

	function officialShopsCtrl($scope, NgTableParams, officialShopsFctry, $filter) {
		$scope.officialShops = [];

		$scope.tableParams = new NgTableParams({
			page: 1, // show first page
			count: 10, // count per page
		}, {
			getData: function(params) {
				return officialShopsFctry.getApplicationList().then(function(data) {
					var officialShops = data.result;
					console.log('officialShops: ', officialShops);

					if ($scope.dataSearch) {
						$scope.officialShops = $filter('filter')(officialShops, $scope.dataSearch);
					} else {
						$scope.officialShops = officialShops;
					}
					console.log('officialShops: ', $scope.officialShops);

					params.total($scope.officialShops.length);
					return ($scope.officialShops.slice((params.page() - 1) * params.count(), params.page() * params.count()));
				});
			}
		});

		$scope.refresh = function() {
			$scope.tableParams.reload();
		};
	}

	officialShopDetailCtrl.$inject = ['_', '$scope', 'officialShopsFctry', '$stateParams'];

	function officialShopDetailCtrl(_, $scope, officialShopsFctry, $stateParams) {
		console.log('$stateParams: ', $stateParams);
		$scope.shop = {};
		$scope.CORFiles = [];
		$scope.LicenseFiles = [];

		$scope.CORFilename = '';
		$scope.LicenseFilename = '';
		$scope.LogoFile = '';

		if ($stateParams) {
			officialShopsFctry.getApplicationDetail($stateParams.shopId).then(function(data) {
				if (data && data.success) {
					console.log('data: ', data.result);
					$scope.shop = data.result;

					if ($scope.shop.shop_cor_filename) {
						var CORFilename = ($scope.shop.shop_cor_filename).split('/');
						$scope.CORFilename = CORFilename[_.size(CORFilename) - 1];
					}

					if ($scope.shop.shop_cor_filename) {
						var LicenseFilename = ($scope.shop.shop_cor_filename).split('/');
						$scope.LicenseFilename = LicenseFilename[_.size(LicenseFilename) - 1];
					}

					if ($scope.shop.shop_official_logo_filename) {
						var LogoFile = ($scope.shop.shop_official_logo_filename).split('/');
						$scope.LogoFile = LogoFile[_.size(LogoFile) - 1];
					}
				}
			}, function(error) {
				console.log('error: ', error);
			});
		}
	}
})();