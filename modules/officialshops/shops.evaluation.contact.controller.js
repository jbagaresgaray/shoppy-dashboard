(function() {
    'use strict';

    angular.module('starter')
        .controller('officialShopContactApplicants', officialShopContactApplicants);

    officialShopContactApplicants.$inject = ['_', '$scope', '$uibModalInstance', 'officialShopsFctry', 'shopId', 'toastr', '$timeout'];

    function officialShopContactApplicants(_, $scope, $uibModalInstance, officialShopsFctry, shopId, toastr, $timeout) {
        $scope.email = {};
        $scope.shop = {};
        $scope.isLoading = true;
        $scope.isSending = false;
        $scope.email.from = 'info@shoppyapp.net';

        function initData() {
            if (shopId) {
                officialShopsFctry.getApplicationDetail(shopId).then(function(data) {
                    if (data && data.success) {
                        $scope.shop = data.result;
                        console.log('data: ', data.result);
                        $scope.email.email = $scope.shop.shop_email;
                    }
                    $scope.isLoading = false;
                }, function(error) {
                    console.log('error: ', error);
                    $scope.isLoading = false;
                });
            }
        }

        initData();

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };


        $scope.sendEmail = function() {
            $scope.isSending = true;
            console.log('sendEmail: ', $scope.email);

            officialShopsFctry.sendEmail($scope.email).then(function(data) {
                console.log('data: ', data);
                if (data && data.success) {
                    toastr.success(data.msg, 'Success');
                    $scope.isSending = false;
                    $timeout(function() {
                        $uibModalInstance.close('save');
                    }, 300);
                } else if (!data.success && _.isArray(data.result)) {
                    _.each(data.result, function(msg) {
                        toastr.warning(msg.msg, 'Warning');
                    });
                    $scope.isSending = false;
                } else {
                    toastr.error(data.msg, 'Error');
                    $scope.isSending = false;
                }
            }, function(error) {
                console.log('error: ', error);
                $scope.isSending = false;
            });
        };
    }
})();