(function() {
	'use strict';

	angular.module('starter')
		.controller('officialShopsEvaluationCtrl', officialShopsEvaluationCtrl)
		.controller('officialShopEvaluationDetailCtrl', officialShopEvaluationDetailCtrl);

	officialShopsEvaluationCtrl.$inject = ['_', '$scope', '$rootScope', 'officialShopsFctry', '$uibModal', 'EVAL_CATEGORY'];

	function officialShopsEvaluationCtrl(_, $scope, $rootScope, officialShopsFctry, $uibModal, EVAL_CATEGORY) {

		$scope.officialShops = [];
		$scope.applicantsList = [];
		$scope.inReviewList = [];
		$scope.inProgressList = [];
		$scope.completedList = [];



		function fetchAPIData() {
			officialShopsFctry.getApplicationEvaluationList().then(function(data) {
				var officialShops = data.result;
				console.log('officialShops: ', officialShops);
				_.each(officialShops, function(row) {
					_.each(row.activity, function(item) {
						item.date = new Date(item.date);
					});

					$scope.officialShops.push({
						official_shop_id: row.official_shop_id,
						official_shop_evaluationId: row.official_shop_evaluationId,
						evaluation: row.evaluation,
						shop_application_date: new Date(row.shop_application_date),
						owner: {
							email: row.owner_email,
							fname: row.owner_fname,
							mname: row.owner_mname,
							lname: row.owner_lname
						},
						shop: {
							name: row.shop_name,
							email: row.shop_email,
							facebook: row.shop_facebook,
							instagram: row.shop_instagram,
							mobileno: row.shop_mobileno,
							website: row.shop_website,
							official_logo: row.shop_official_logo,
							ref_code: row.shop_ref_code,
							cor_file: row.shop_cor_file,
							cor_number: row.shop_cor_number,
							distributor_license: row.shop_distributor_license,
							distributor_license_number: row.shop_distributor_license_number,
						}
					});
				});
				console.log('officialShops: ', $scope.officialShops);

				$scope.applicantsList = _.filter($scope.officialShops, function(row) {
					return row.evaluation.eval_categoryID === EVAL_CATEGORY.applicants;
				});

				$scope.inReviewList = _.filter($scope.officialShops, function(row) {
					return row.evaluation.eval_categoryID === EVAL_CATEGORY.inReview;
				});

				$scope.inProgressList = _.filter($scope.officialShops, function(row) {
					return row.evaluation.eval_categoryID === EVAL_CATEGORY.forApproval;
				});

				$scope.completedList = _.filter($scope.officialShops, function(row) {
					return row.evaluation.eval_categoryID === EVAL_CATEGORY.completed;
				});

				/*console.log('applicantsList: ', $scope.applicantsList);
				console.log('inReviewList: ', $scope.inReviewList);
				console.log('inProgressList: ', $scope.inProgressList);
				console.log('completedList: ', $scope.completedList);*/
			});
		}

		fetchAPIData();


		$scope.sortableOptions = {
			// connectWith: ".connectList",
			placeholder: 'app',
			items: 'li:not(.not-sortable)'
		};

		$scope.applicantsOnUpdated = function(ev, ui) {
			console.log('applicantsOnUpdated ', ui.item.sortable);
			console.log('applicantsOnUpdated ', ui.item.scope());
		};

		$scope.applicantsOnReceived = function(ev, ui) {
			console.log('applicantsOnReceived ', ui.item.scope());
		};

		$scope.inReviewOnUpdate = function(ev, ui) {
			console.log('inReviewOnUpdate ', ui.item.sortable);
			console.log('inReviewOnUpdate ', ui.item.scope());
			console.log('inReviewList: ', $scope.inReviewList);
		};

		$scope.inReviewOnReceived = function(ev, ui) {
			console.log('inReviewOnReceived ', ui.item.sortable);
			console.log('inReviewOnReceived ', ui.item.scope());
			console.log('inReviewList: ', $scope.inReviewList);
		};

		$scope.inProgressOnReceived = function(ev, ui) {
			console.log('inProgressOnReceived ', ui.item.scope());
		};

		$scope.completedOnReceived = function(ev, ui) {
			console.log('completedOnReceived ', ui.item.scope());
		};



		$scope.updateApplication = function(application) {
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'modules/officialshops/shop_detail.modal.html',
				controller: 'officialShopEvaluationDetailCtrl',
				windowClass: 'modal-parent',
				size: 'lg',
				resolve: {
					shopId: function() {
						return application.official_shop_id;
					}
				}
			});

			modalInstance.result.then(function(selectedItem) {
				if (selectedItem) {
					fetchAPIData();
				}
			}, function() {});
		};

		$scope.viewApplication = function(application) {
			console.log('application: ', application);
		};

		$scope.contactApplicant = function(application) {
			var modalScope = $rootScope.$new();
			modalScope.modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'modules/officialshops/contact.applicant.modal.html',
				size: 'md',
				controller: 'officialShopContactApplicants',
				resolve: {
					shopId: function() {
						return application.official_shop_id;
					}
				}
			});

			modalScope.modalInstance.result.then(function(selectedItem) {
				if (selectedItem) {
					fetchAPIData();
				}
			}, function() {});
		};

	}


	officialShopEvaluationDetailCtrl.$inject = ['_', '$scope', '$rootScope', 'officialShopsFctry', 'shopId', 'EVAL_CATEGORY_LIST', 'EVAL_STATUS', 'EVAL_CATEGORY', 'auth', '$uibModal', '$uibModalInstance', 'ngDialog', 'toastr', '$timeout'];

	function officialShopEvaluationDetailCtrl(_, $scope, $rootScope, officialShopsFctry, shopId, EVAL_CATEGORY_LIST, EVAL_STATUS, EVAL_CATEGORY, auth, $uibModal, $uibModalInstance, ngDialog, toastr, $timeout) {
		$scope.shop = {};
		$scope.CORFiles = [];
		$scope.LicenseFiles = [];

		$scope.CORFilename = '';
		$scope.LicenseFilename = '';
		$scope.LogoFile = '';

		$scope.evaluationCategory = EVAL_CATEGORY_LIST;
		if (auth.getUser()) {
			$scope.currentUser = auth.getUser().user;
		}

		function initData() {
			if (shopId) {
				officialShopsFctry.getApplicationDetail(shopId).then(function(data) {
					if (data && data.success) {
						console.log('data: ', data.result);
						$scope.shop = data.result;

						if ($scope.shop.shop_cor_filename) {
							var CORFilename = ($scope.shop.shop_cor_filename).split('/');
							$scope.CORFilename = CORFilename[_.size(CORFilename) - 1];
						}

						if ($scope.shop.shop_cor_filename) {
							var LicenseFilename = ($scope.shop.shop_cor_filename).split('/');
							$scope.LicenseFilename = LicenseFilename[_.size(LicenseFilename) - 1];
						}

						if ($scope.shop.shop_official_logo_filename) {
							var LogoFile = ($scope.shop.shop_official_logo_filename).split('/');
							$scope.LogoFile = LogoFile[_.size(LogoFile) - 1];
						}
					}
				}, function(error) {
					console.log('error: ', error);
				});
			}
		}

		initData();

		$scope.contactApplicant = function() {
			var modalScope = $rootScope.$new();
			modalScope.modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'modules/officialshops/contact.applicant.modal.html',
				size: 'md',
				controller: 'officialShopContactApplicants',
			});

			modalScope.modalInstance.result.then(function(selectedItem) {
				if (selectedItem) {
					initData();
				}
			}, function() {});
		};


		function submitEvaluation(evalData) {
			officialShopsFctry.updateApplicationEvaluation(evalData).then(function(data) {
				if (data && data.success) {
					toastr.success(data.msg, 'Success');
					$timeout(function() {
						initData();
					}, 300);
				} else if (!data.success && _.isArray(data.result)) {
					_.each(data.result, function(msg) {
						toastr.warning(msg.msg, 'Warning');
					});
				} else {
					toastr.error(data.msg, 'Error');
				}
			}, function(error) {
				console.log('Error: ', error);
			});
		}

		function submitApproval(shopId, evalData) {
			officialShopsFctry.approveOfficialShopApplication(shopId, evalData).then(function(data) {
				if (data && data.success) {
					toastr.success(data.msg, 'Success');
					$timeout(function() {
						initData();
					}, 300);
				} else if (!data.success && _.isArray(data.result)) {
					_.each(data.result, function(msg) {
						toastr.warning(msg.msg, 'Warning');
					});
				} else {
					toastr.error(data.msg, 'Error');
				}
			}, function(error) {
				console.log('Error: ', error);
			});
		}

		$scope.reviewApplication = function() {
			ngDialog.openConfirm({
				templateUrl: './modules/dialogs/confirm.dialog.html',
				scope: $scope,
				className: 'ngdialog-theme-default'
			}).then(function() {
				var content = '<b>SHOP NAME:</b> ' + $scope.shop.shop_name + '<br> <b>SELLER NAME:</b> ' + $scope.shop.sellerName + '<br> <b>REVIEWED DATE:</b> ' + new Date().toISOString() + ' <br> <b>REVIEWED BY:</b> ' + $scope.currentUser.firstname + ' ' + $scope.currentUser.lastname;
				submitEvaluation({
					official_shop_id: $scope.shop.official_shop_id,
					date: new Date().toISOString(),
					statusClass: EVAL_STATUS.danger,
					tagName: JSON.stringify($scope.currentUser),
					content: content,
					eval_categoryID: EVAL_CATEGORY.inReview
				});
			}, function() {});
		};

		$scope.forApprovalApplication = function() {
			ngDialog.openConfirm({
				templateUrl: './modules/dialogs/confirm.dialog.html',
				scope: $scope,
				className: 'ngdialog-theme-default'
			}).then(function() {
				var content = '<b>SHOP NAME:</b> ' + $scope.shop.shop_name + '<br> <b>SELLER NAME:</b> ' + $scope.shop.sellerName + '<br> <b>CHECKED DATE:</b> ' + new Date().toISOString() + ' <br> <b>CHECKED BY:</b> ' + $scope.currentUser.firstname + ' ' + $scope.currentUser.lastname;
				submitEvaluation({
					official_shop_id: $scope.shop.official_shop_id,
					date: new Date().toISOString(),
					statusClass: EVAL_STATUS.warning,
					tagName: JSON.stringify($scope.currentUser),
					content: content,
					eval_categoryID: EVAL_CATEGORY.forApproval
				});
			}, function() {});
		};


		$scope.approveApplication = function() {
			ngDialog.openConfirm({
				templateUrl: './modules/dialogs/confirm.dialog.html',
				scope: $scope,
				className: 'ngdialog-theme-default'
			}).then(function() {
				var content = '<b>SHOP NAME:</b> ' + $scope.shop.shop_name + '<br> <b>SELLER NAME:</b> ' + $scope.shop.sellerName + '<br> <b>APPROVED DATE:</b> ' + new Date().toISOString() + ' <br> <b>APPROVED BY:</b> ' + $scope.currentUser.firstname + ' ' + $scope.currentUser.lastname;
				submitApproval($scope.shop.official_shop_id, {
					official_shop_id: $scope.shop.official_shop_id,
					date: new Date().toISOString(),
					statusClass: EVAL_STATUS.success,
					tagName: JSON.stringify($scope.currentUser),
					content: content,
					eval_categoryID: EVAL_CATEGORY.completed,
					shop_appoved_date: new Date().toISOString()
				});
			}, function() {

			});
		};


		$scope.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
	}

})();