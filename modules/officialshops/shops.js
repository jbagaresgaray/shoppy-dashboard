(function() {
    'use strict';

    angular.module('starter')
        .config(config)
        .factory('officialShopsFctry', officialShopsFctry);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('app.officialshops', {
                url: '/officialshops',
                templateUrl: 'modules/officialshops/shops.html',
                data: {
                    pageTitle: 'Official Shops'
                },
                controller: 'officialShopsCtrl',
            })
            .state('app.officialshops_eval', {
                url: '/officialshops/evaluation',
                templateUrl: 'modules/officialshops/evaluation.html',
                data: {
                    pageTitle: 'Official Shops'
                },
                controller: 'officialShopsEvaluationCtrl',
            })
            .state('app.officialshops_info', {
                url: '/officialshops/:shopId/info',
                templateUrl: 'modules/officialshops/shop_detail.html',
                data: {
                    pageTitle: 'Official Shops'
                },
                controller: 'officialShopDetailCtrl'
            });

    }

    officialShopsFctry.$inject = ['Restangular'];

    function officialShopsFctry(Restangular) {
        return {
            getApplicationList: function(params) {
                return Restangular.all('officialshop').customGET('', params).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getApplicationDetail: function(shopId) {
                return Restangular.all('officialshop').customGET(shopId).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getApplicationEvaluationList: function(params) {
                return Restangular.all('officialshop-evaluations').customGET('', params).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            updateApplicationEvaluation: function(data) {
                return Restangular.all('officialshop-evaluations').customPOST(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            approveOfficialShopApplication: function(shopId, data) {
                return Restangular.all('officialshop/' + shopId + '/approved').customPOST(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            sendEmail: function(data) {
                return Restangular.all('sendMail').customPOST(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            }
        };
    }
})();