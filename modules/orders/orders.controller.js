(function() {
    'use strict';

    angular.module('starter')
        .controller('ordersCtrl', ordersCtrl);

    ordersCtrl.$inject = ['_', '$scope', '$uibModal', 'ordersFctry', 'NgTableParams', '$filter', 'ngDialog', '$timeout', 'toastr', '$state'];

    function ordersCtrl(_, $scope, $uibModal, ordersFctry, NgTableParams, $filter, ngDialog, $timeout, toastr, $state) {
        console.log('ordersCtrl');

        $scope.orders = [];
        $scope.ordersCopy = [];
        $scope.slides = [];

        $scope.dataSearch = '';
        $scope.showApproveIco = false;
        $scope.radioModel1 = 'all';

        $scope.slides = [{
            id: 'pay',
            name: 'To Pay'
        }, {
            id: 'unpaid',
            name: 'Unpaid',
        }, {
            id: 'ship',
            name: 'To Ship'
        }, {
            id: 'shipping',
            name: 'Shipping'
        }, {
            id: 'receive',
            name: 'Receive'
        }, {
            id: 'completed',
            name: 'Completed'
        }, {
            id: 'cancelled',
            name: 'Cancelled'
        }, {
            id: 'return',
            name: 'Return/Refund'
        }];

        _.each($scope.slides, function(row) {
            row.count = 0;
        });

        $scope.tableParams = new NgTableParams({
            page: 1, // show first page
            count: 10, // count per page
        }, {
            getData: function(params) {
                return ordersFctry.getOrdersList(null).then(function(data) {
                    if (data && data.success) {
                        var orders = data.result;
                        console.log('orders: ', orders);

                        _.each(orders, function(row) {
                            if (row.isCOD === 1) {
                                if (row.isPrepared === 0 && row.isShipped === 0 && row.isReceived === 0 && row.isCancelled === 0 && row.isReturned === 0) {
                                    row.status = 'UNPAID';
                                } else if (row.isPrepared === 1 && row.isCancelled === 0) {
                                    row.status = 'CONFIRMED';
                                } else if (row.isPrepared === 1 && row.isShipped === 1 && row.isReceived === 0 && row.isCancelled === 0) {
                                    row.status = 'IN-TRANSIT';
                                } else if (row.isPrepared === 1 && row.isShipped === 1 && row.isReceived === 1 && row.isCancelled === 0) {
                                    row.status = 'DELIVERED';
                                } else if (row.isCancelled === 1 && row.isReturned === 0) {
                                    row.status = 'CANCELLED';
                                } else if (row.isReturned === 1) {
                                    row.status = 'RETURNED';
                                }
                            } else {
                                if (row.isPaid === 1) {
                                    if (row.isPrepared === 0 && row.isShipped === 0 && row.isReceived === 0 && row.isCancelled === 0 && row.isReturned === 0) {
                                        row.status = 'UNPAID';
                                    } else if (row.isPrepared === 1 && row.isCancelled === 0) {
                                        row.status = 'CONFIRMED';
                                    } else if (row.isPrepared === 1 && row.isShipped === 1 && row.isReceived === 0 && row.isCancelled === 0) {
                                        row.status = 'IN-TRANSIT';
                                    } else if (row.isPrepared === 1 && row.isShipped === 1 && row.isReceived === 1 && row.isCancelled === 0) {
                                        row.status = 'DELIVERED';
                                    } else if (row.isCancelled === 1 && row.isReturned === 0) {
                                        row.status = 'CANCELLED';
                                    } else if (row.isReturned === 1) {
                                        row.status = 'RETURNED';
                                    }
                                }
                            }
                        });

                        _.each($scope.slides, function(row) {
                            if (row.id === 'pay' || row.id === 'unpaid') {
                                row.count = _.size(_.filter(orders, { 'status': 'UNPAID' }));
                            } else if (row.id === 'ship') {
                                row.count = _.size(_.filter(orders, { 'status': 'CONFIRMED' }));
                            } else if (row.id === 'receive' || row.id === 'shipping') {
                                row.count = _.size(_.filter(orders, { 'status': 'IN-TRANSIT' }));
                            }
                        });



                        if ($scope.dataSearch) {
                            $scope.orders = $filter('filter')(orders, $scope.dataSearch);
                        } else {
                            $scope.orders = orders;
                        }
                        $scope.ordersCopy = angular.copy($scope.orders);

                        params.total($scope.orders.length);
                        return ($scope.orders.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    }
                });
            }
        });

        $scope.filterAction = function(action) {
            console.log('action: ', action);
            $scope.radioModel1 = action;

            if (action === 'all') {
                action = null;
            }

            $scope.getOrdersList(action);
        };


        $scope.getOrdersList = function(type) {
            $scope.tableParams = new NgTableParams({
                page: 1, // show first page
                count: 10, // count per page
            }, {
                getData: function(params) {
                    return ordersFctry.getOrdersList(type).then(function(data) {
                        if (data && data.success) {
                            var orders = data.result;
                            console.log('orders: ', orders);
                            if ($scope.dataSearch) {
                                $scope.orders = $filter('filter')(orders, $scope.dataSearch);
                            } else {
                                $scope.orders = orders;
                            }

                            _.each($scope.slides, function(row) {
                                if (row.id === type) {
                                    row.count = _.size(orders);
                                }
                            });

                            params.total($scope.orders.length);
                            return ($scope.orders.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                        }
                    });
                }
            });
        };

        $scope.refresh = function() {
            $scope.tableParams.reload();
        };

        $scope.viewOrderShippingInfo = function(order) {
            console.log('order: ', order);
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/orders/order.shipping.modal.html',
                controller: 'orderShippingModalCtrl',
                size: 'md'
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.refresh();
                }
            }, function() {});
        };

        $scope.paidOrder = function(order) {
            console.log('order: ', order);
            $scope.modal = {
                title: 'Pay Order',
                message: 'Mark order as PAID?'
            };
            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {

            }, function() {

            });
        };

        $scope.viewOrder = function(order) {
            $state.go('app.orders_detail', {
                orderId: order.uuid
            });
        };
    }
})();