(function() {
    'use strict';

    angular.module('starter')
        .config(config)
        .factory('ordersFctry', ordersFctry);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('app.orders', {
                url: '/orders',
                templateUrl: 'modules/orders/orders.html',
                data: {
                    pageTitle: 'Orders'
                },
                controller: 'ordersCtrl',
            })
            .state('app.orders_detail', {
                url: '/orders/:orderId',
                templateUrl: 'modules/orders/order.detail.html',
                data: {
                    pageTitle: 'Orders'
                },
                controller: 'orderDetailsCtrl',
            });

    }

    ordersFctry.$inject = ['Restangular'];

    function ordersFctry(Restangular) {
        return {
            getOrdersList: function(params) {
                return Restangular.all('orders').customGET(null,{
                    action: params
                }).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getOrderDetails: function(orderId) {
                return Restangular.all('orders').customGET(orderId).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getOrderDetailsByBatchNum: function(batchId){
                return Restangular.all('orders/' + batchId + '/batch').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            }
        };
    }
})();