(function() {
    'use strict';

    angular.module('starter')
        .controller('orderShippingModalCtrl', orderShippingModalCtrl);

    orderShippingModalCtrl.$inject = ['_', '$scope', '$uibModalInstance'];

    function orderShippingModalCtrl(_, $scope, $uibModalInstance) {

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();


(function() {
    'use strict';

    angular.module('starter')
        .controller('orderDetailsCtrl', orderDetailsCtrl);

    orderDetailsCtrl.$inject = ['_', '$scope', 'ordersFctry', '$stateParams'];

    function orderDetailsCtrl(_, $scope, ordersFctry, $stateParams) {
        $scope.order = {};

        if($stateParams.orderId){
            ordersFctry.getOrderDetails($stateParams.orderId).then(function(data){
                if(data && data.success){
                    console.log('data: ',data.result);
                    $scope.order = data.result;
                }
            });
        }

    }
})();