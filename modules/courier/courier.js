(function() {
    'use strict';

    angular.module('starter')
        .config(config)
        .factory('courierFctry', courierFctry);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('app.courier', {
                url: '/courier',
                templateUrl: 'modules/courier/courier.html',
                data: {
                    pageTitle: 'Shipping Courier'
                },
                controller: 'courierCtrl',
            });

    }

    courierFctry.$inject = ['Restangular'];

    function courierFctry(Restangular) {
        return {
            getAllCourier: function() {
                return Restangular.all('courier').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            createCourier: function(data) {
                return Restangular.all('courier').customPOST(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getCourierById: function(courierId) {
                return Restangular.all('courier').customGET(courierId).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            deleteCourierById: function(courierId) {
                return Restangular.all('courier').customDELETE(courierId).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            updateCourierById: function(courierId, data) {
                return Restangular.all('courier/' + courierId).customPUT(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            }
        };
    }
})();