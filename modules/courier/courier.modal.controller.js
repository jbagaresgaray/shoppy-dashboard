(function() {
    'use strict';

    angular.module('starter')
        .controller('courierModalCtrl', courierModalCtrl);

    courierModalCtrl.$inject = ['_', '$scope', '$uibModalInstance', 'courierFctry', 'courierId', 'toastr', '$timeout'];

    function courierModalCtrl(_, $scope, $uibModalInstance, courierFctry, courierId, toastr, $timeout) {

        $scope.courier = {};

        if (courierId) {
            courierFctry.getCourierById(courierId).then(function(data) {
                if (data && data.success) {
                    var courier = data.result;
                    if (!_.isEmpty(courier)) {
                        $scope.courier = courier;
                    }
                }
            });
        }

        $scope.save = function() {
            if (courierId) {
                courierFctry.updateCourierById(courierId, $scope.courier).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $timeout(function() {
                            $uibModalInstance.close('save');
                        }, 300);
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else {
                courierFctry.createCourier($scope.courier).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $timeout(function() {
                            $uibModalInstance.close('save');
                        }, 300);
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();