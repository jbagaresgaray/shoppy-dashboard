(function() {
    'use strict';

    angular.module('starter')
        .controller('courierCtrl', courierCtrl);

    courierCtrl.$inject = ['$scope', '$uibModal', 'courierFctry', 'NgTableParams', 'ngDialog', '$filter', 'toastr'];

    function courierCtrl($scope, $uibModal, courierFctry, NgTableParams, ngDialog, $filter, toastr) {

        $scope.couriers = [];
        $scope.dataSearch = '';

        $scope.tableParams = new NgTableParams({
            page: 1, // show first page
            count: 10, // count per page
        }, {
            getData: function(params) {
                return courierFctry.getAllCourier().then(function(data) {
                    if (data && data.success) {
                        var couriers = data.result;

                        if ($scope.dataSearch) {
                            $scope.couriers = $filter('filter')(couriers, $scope.dataSearch);
                        } else {
                            $scope.couriers = couriers;
                        }

                        params.total($scope.couriers.length);
                        return ($scope.couriers.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    }
                });
            }
        });

        $scope.addCourier = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/courier/courier.modal.html',
                controller: 'courierModalCtrl',
                size: 'md',
                resolve: {
                    courierId: function() {
                        return null;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.refresh();
                }
            }, function() {});
        };


        $scope.updateCourier = function(item) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/courier/courier.modal.html',
                controller: 'courierModalCtrl',
                size: 'md',
                resolve: {
                    courierId: function() {
                        return item.shippingCourierId;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.refresh();
                }
            }, function() {});
        };

        $scope.deleteSelected = function(item) {
            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                courierFctry.deleteCourierById(item.shippingCourierId).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.tableParams.reload();
                    }
                });
            }, function() {

            });
        };

        $scope.refresh = function() {
            $scope.tableParams.reload();
        };

        
    }
})();