(function() {
    'use strict';

    angular.module('starter')
        .config(config)
        .factory('paymentProviderFctry', paymentProviderFctry);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('app.paymentprovider', {
                url: '/paymentprovider',
                templateUrl: 'modules/payment_provider/provider.html',
                data: {
                    pageTitle: 'Payment Provider'
                },
                controller: 'paymentProviderCtrl',
            });

    }

    paymentProviderFctry.$inject = ['Restangular'];

    function paymentProviderFctry(Restangular) {
        return {
            getAllPaymentProvider: function() {
                return Restangular.all('paymentprovider').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            createPaymentProvider: function(data) {
                return Restangular.all('paymentprovider').customPOST(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getPaymentProviderById: function(providerId) {
                return Restangular.all('paymentprovider').customGET(providerId).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            deletePaymentProviderById: function(providerId) {
                return Restangular.all('paymentprovider').customDELETE(providerId).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            updatePaymentProviderById: function(providerId, data) {
                return Restangular.all('paymentprovider/' + providerId).customPUT(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            }
        };
    }
})();