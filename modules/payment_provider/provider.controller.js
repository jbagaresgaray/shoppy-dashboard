(function() {
    'use strict';

    angular.module('starter')
        .controller('paymentProviderCtrl', paymentProviderCtrl);

    paymentProviderCtrl.$inject = ['$scope', '$uibModal', 'paymentProviderFctry', 'NgTableParams', 'ngDialog', '$filter', 'toastr'];

    function paymentProviderCtrl($scope, $uibModal, paymentProviderFctry, NgTableParams, ngDialog, $filter, toastr) {

        $scope.providers = [];
        $scope.dataSearch = '';

        $scope.tableParams = new NgTableParams({
            page: 1, // show first page
            count: 10, // count per page
        }, {
            getData: function(params) {
                return paymentProviderFctry.getAllPaymentProvider().then(function(data) {
                    if (data && data.success) {
                        var providers = data.result;

                        if ($scope.dataSearch) {
                            $scope.providers = $filter('filter')(providers, $scope.dataSearch);
                        } else {
                            $scope.providers = providers;
                        }

                        params.total($scope.providers.length);
                        return ($scope.providers.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    }
                });
            }
        });


        $scope.addProvider = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/payment_provider/provider.modal.html',
                controller: 'paymentProviderModalCtrl',
                size: 'md',
                resolve: {
                    providerId: function() {
                        return null;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.refresh();
                }
            }, function() {});
        };


        $scope.updateProvider = function(item) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/payment_provider/provider.modal.html',
                controller: 'paymentProviderModalCtrl',
                size: 'md',
                resolve: {
                    providerId: function() {
                        return item.paymentProvidersId;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.refresh();
                }
            }, function() {});
        };

        $scope.deleteSelected = function(item) {
            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                paymentProviderFctry.deletePaymentProviderById(item.paymentProvidersId).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.tableParams.reload();
                    }
                });
            }, function() {

            });
        };

        $scope.refresh = function() {
            $scope.tableParams.reload();
        };
    }
})();