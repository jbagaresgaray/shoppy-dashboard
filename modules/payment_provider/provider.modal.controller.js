(function() {
    'use strict';

    angular.module('starter')
        .controller('paymentProviderModalCtrl', paymentProviderModalCtrl);

    paymentProviderModalCtrl.$inject = ['_', '$scope', '$uibModalInstance', 'paymentProviderFctry', 'providerId', 'toastr', '$timeout'];

    function paymentProviderModalCtrl(_, $scope, $uibModalInstance, paymentProviderFctry, providerId, toastr, $timeout) {
        $scope.provider = {};

        if (providerId) {
            paymentProviderFctry.getPaymentProviderById(providerId).then(function(data) {
                if (data && data.success) {
                    var provider = data.result;
                    if (!_.isEmpty(provider)) {
                        $scope.provider = provider;
                    }
                }
            });
        }

        $scope.save = function() {
            if (providerId) {
                paymentProviderFctry.updatePaymentProviderById(providerId, $scope.provider).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $timeout(function() {
                            $uibModalInstance.close('save');
                        }, 300);
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else {
                paymentProviderFctry.createPaymentProvider($scope.provider).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $timeout(function() {
                            $uibModalInstance.close('save');
                        }, 300);
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();