(function() {
    'use strict';

    angular.module('starter')
        .config(loginConfig)
        .factory('auth', auth);

    loginConfig.$inject = ['$stateProvider'];

    function loginConfig($stateProvider) {
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'modules/login/login.html',
                controller: 'LoginCtrl',
                data: { pageTitle: 'Login', specialClass: 'gray-bg' }
            });
    }

    auth.$inject = ['Restangular', '$http', 'localStorageService', '$window'];

    function auth(Restangular, $http, localStorageService, $window) {
        return {
            isAuthenticated: false,
            login: function(username, password) {
                var authdata = $window.btoa(username + ':' + password);
                console.log('authdata: ', authdata);
                localStorageService.set('authdata', authdata);
                $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
                return Restangular.all('authenticate').customPOST().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            storeUser: function(user) {
                localStorageService.set('user', user);
                return true;
            },
            getUser: function() {
                return localStorageService.get('user');
            },
            logout: function() {
                return Restangular.all('logout').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            }
        };
    }
})();