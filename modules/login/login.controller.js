(function() {
    'use strict';

    angular.module('starter')
        .controller('LoginCtrl', ['$scope', '$state', 'auth', 'toastr', '$timeout', '_',
            function($scope, $state, auth, toastr, $timeout, _) {

                if (auth.getUser()) {
                    $state.go('dashboards.dashboard_1');
                }

                $scope.username = 'user123';
                $scope.password = '123456789';
                $scope.submitting = false;

                $scope.loginApp = function() {
                    $scope.submitting = true;
                    auth.login($scope.username, $scope.password).then(function(resp) {
                        console.log('resp: ', resp);
                        if (!resp.success && _.isArray(resp.result)) {
                            _.each(resp.result, function(row) {
                                toastr.error(row.msg, 'Error');
                            });
                            return;
                        }
                        if (resp.success) {
                            auth.storeUser(resp.result);

                            $timeout(function() {
                                $state.go('dashboards.dashboard_1');
                            }, 300);
                        } else if (!resp.success && _.isArray(resp.result)) {
                            _.each(resp.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            $scope.submitting = false;
                            toastr.error(resp.msg, 'Error');
                        }
                    });
                };

            }
        ]);
})();