(function() {
    'use strict';

    angular.module('starter')
        .config(config)
        .factory('sellersFctry', sellersFctry);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('app.sellers', {
                url: '/sellers',
                templateUrl: 'modules/sellers/sellers.html',
                data: {
                    pageTitle: 'Sellers'
                },
                controller: 'sellerCtrl',
            })
            .state('app.sellers_eval', {
                url: '/sellers/evaluation',
                templateUrl: 'modules/sellers/evaluation.html',
                data: {
                    pageTitle: 'Sellers'
                },
                controller: 'sellersEvaluationCtrl'
            });

    }

    sellersFctry.$inject = ['Restangular'];

    function sellersFctry(Restangular) {
        return {
            getAllSellers: function() {
                return Restangular.all('seller').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getAllApplicants: function() {
                return Restangular.all('seller/applicants').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            approveSellerApplication: function(data){
                return Restangular.all('seller/' + data.uuid + '/approval').customPOST({
                    code: data.sellerApprovalCode
                }).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            }
        };
    }
})();