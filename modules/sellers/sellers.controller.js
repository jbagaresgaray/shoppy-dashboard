(function() {
    'use strict';

    angular.module('starter')
        .controller('sellerCtrl', sellerCtrl);

    sellerCtrl.$inject = ['_', '$scope', '$uibModal', 'sellersFctry', 'NgTableParams', '$filter', 'ngDialog', '$timeout', 'toastr'];

    function sellerCtrl(_, $scope, $uibModal, sellersFctry, NgTableParams, $filter, ngDialog, $timeout, toastr) {
        console.log('sellerCtrl');

        $scope.sellers = [];
        $scope.dataSearch = '';
        $scope.radioModel = 'Sellers';
        $scope.showApproveIco = false;

        $scope.tableParams = new NgTableParams({
            page: 1, // show first page
            count: 10, // count per page
        }, {
            getData: function(params) {
                return sellersFctry.getAllSellers().then(function(data) {
                    if (data && data.success) {
                        var sellers = data.result;
                        if ($scope.dataSearch) {
                            $scope.sellers = $filter('filter')(sellers, $scope.dataSearch);
                        } else {
                            $scope.sellers = sellers;
                        }

                        _.each($scope.sellers, function(row) {
                            row.sellerApprovalDate = new Date(row.sellerApprovalDate);
                        });

                        params.total($scope.sellers.length);
                        return ($scope.sellers.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    }
                });
            }
        });

        $scope.addSeller = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/seller/seller.modal.html',
                controller: 'sellerModalCtrl',
                size: 'md',
            });

            modalInstance.result.then(function() {

            }, function() {
                console.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.refresh = function() {
            $scope.tableParams.reload();

            $scope.$parent.refreshSellers();
        };

        $scope.showSellers = function() {
            $scope.showApproveIco = false;

            $scope.tableParams = new NgTableParams({
                page: 1, // show first page
                count: 10, // count per page
            }, {
                getData: function(params) {
                    return sellersFctry.getAllSellers().then(function(data) {
                        if (data && data.success) {
                            var sellers = data.result;
                            if ($scope.dataSearch) {
                                $scope.sellers = $filter('filter')(sellers, $scope.dataSearch);
                            } else {
                                $scope.sellers = sellers;
                            }

                            _.each($scope.sellers, function(row) {
                                row.sellerApprovalDate = new Date(row.sellerApprovalDate);
                            });

                            params.total($scope.sellers.length);
                            return ($scope.sellers.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                        }
                    });
                }
            });
        };

        $scope.showApplicants = function() {
            $scope.showApproveIco = true;

            $scope.tableParams = new NgTableParams({
                page: 1, // show first page
                count: 10, // count per page
            }, {
                getData: function(params) {
                    return sellersFctry.getAllApplicants().then(function(data) {
                        if (data && data.success) {
                            var sellers = data.result;
                            if ($scope.dataSearch) {
                                $scope.sellers = $filter('filter')(sellers, $scope.dataSearch);
                            } else {
                                $scope.sellers = sellers;
                            }

                            _.each($scope.sellers, function(row) {
                                row.sellerApplicationDate = new Date(row.sellerApplicationDate);
                            });

                            params.total($scope.sellers.length);
                            return ($scope.sellers.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                        }
                    });
                }
            });
        };

        $scope.approveApplicant = function(item) {
            $scope.modal = {
                title: 'Approve Application as Seller',
                message: 'Are you sure to approve application?'
            };

            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                console.log('user: ', item);
                sellersFctry.approveSellerApplication(item).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');

                        $timeout(function() {
                            $scope.refresh();
                            $scope.$parent.refreshSellers();
                        }, 300);
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }, function() {

            });
        };

        $scope.viewApplication = function(item) {
            console.log('application: ', item);
        };
    }
})();