(function() {
    'use strict';

    angular.module('starter')
        .controller('notificationsCtrl', notificationsCtrl)
        .controller('notificationCtrl', notificationCtrl)
        .controller('createNotifCtrl', createNotifCtrl);

    notificationsCtrl.$inject = ['_', '$scope', '$state', 'notificationFctry', 'ngDialog', 'toastr','$window'];

    function notificationsCtrl(_, $scope, $state, notificationFctry, ngDialog, toastr,$window) {

        $scope.notifications = [];

        notificationFctry.getAllNotifications().then(function(data) {
            if (data && data.success) {
                $scope.notifications = data.result.notifications;
                _.each($scope.notifications, function(row) {
                    row.sentAt = new Date(row.send_after * 1000);
                    row.queuedAt = new Date(row.queued_at * 1000);
                });
            }
        });

        $scope.composeNewsletter = function() {
            $state.go('app.composenotif');
        };

        $scope.viewNotif = function(Id) {
            $state.go('app.pushnotifdetail', {
                notifId: Id
            });
        };

        $scope.deleteNotif = function(Id,event) {
            event.preventDefault();
            event.stopPropagation();

            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                notificationFctry.deleteNotification(Id).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'SUCCESS');
                        setTimeout(function(){
                            $window.location.reload();
                        }, 600);
                    }else{
                        toastr.warning(data.msg, 'WARNING');
                        return;
                    }
                });
            }, function() {

            });
        };

        $scope.refresh = function() {
            $scope.notifications = [];

            notificationFctry.getAllNotifications().then(function(data) {
                if (data && data.success) {
                    $scope.notifications = data.result.notifications;
                    _.each($scope.notifications, function(row) {
                        row.sentAt = new Date(row.send_after * 1000);
                        row.queuedAt = new Date(row.queued_at * 1000);
                    });
                }
            });
        };
    }

    notificationCtrl.$inject = ['_', '$scope', '$stateParams', 'notificationFctry'];

    function notificationCtrl(_, $scope, $stateParams, notificationFctry) {

        $scope.notification = {};

        if ($stateParams.notifId) {
            notificationFctry.getNotification($stateParams.notifId).then(function(data) {
                if (data && data.success) {
                    $scope.notification = data.result;
                    $scope.notification.sentAt = new Date($scope.notification.send_after * 1000);
                    $scope.notification.queuedAt = new Date($scope.notification.queued_at * 1000);

                    console.log('notification: ', $scope.notification);
                }
            },function(error){
                console.log('error: ', error);
            });
        }
    }

    createNotifCtrl.$inject = ['_', '$scope', '$state', 'notificationFctry', 'toastr'];

    function createNotifCtrl(_, $scope, $state, notificationFctry, toastr) {

        $scope.notif = {};
        $scope.isSending = false;

        $scope.sendNotif = function() {
            console.log('notif: ', $scope.notif);
            $scope.isSending = true;
            notificationFctry.createNotif($scope.notif).then(function(data) {
                console.log('data: ', data);
                if (data && data.success) {
                    toastr.success(data.msg, 'Success');
                    $state.go('app.pushnotif');
                }
            }, function(error){
                console.log('error: ', error);
            });
        };
    }
})();