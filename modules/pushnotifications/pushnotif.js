(function() {
    'use strict';

    angular.module('starter')
        .config(config)
        .factory('notificationFctry', notificationFctry);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('app.pushnotif', {
                url: '/pushnotifications',
                templateUrl: 'modules/pushnotifications/notifications.html',
                controller: 'notificationsCtrl',
                data: {
                    pageTitle: 'Push Notification'
                },
            })
            .state('app.pushnotifdetail', {
                url: '/pushnotifications/:notifId',
                templateUrl: 'modules/pushnotifications/notification.html',
                data: {
                    pageTitle: 'Notification'
                },
                controller: 'notificationCtrl'
            })
            .state('app.composenotif', {
                url: '/pushnotifications/compose',
                templateUrl: 'modules/pushnotifications/pushnotif.html',
                controller:'createNotifCtrl',
                data: {
                    pageTitle: 'Compose Notification'
                }
            });

    }

    notificationFctry.$inject = ['Restangular'];

    function notificationFctry(Restangular) {
        return {
            getAllNotifications: function() {
                return Restangular.all('push').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            createNotif: function(data) {
                return Restangular.all('push').customPOST(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getNotification: function(notificationId) {
                return Restangular.all('push').customGET(notificationId).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            deleteNotification: function(notificationId){
                return Restangular.all('push').customDELETE(notificationId).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            }
        };
    }
})();