(function() {
    'use strict';

    angular.module('starter')
        .controller('brandsCtrl', brandsCtrl);

    brandsCtrl.$inject = ['$scope', '$uibModal', 'brandFctory', 'NgTableParams', 'ngDialog', 'toastr', '$filter'];

    function brandsCtrl($scope, $uibModal, brandFctory, NgTableParams, ngDialog, toastr, $filter) {

        $scope.brands = [];

        $scope.tableParams = new NgTableParams({
            page: 1, // show first page
            count: 10, // count per page
        }, {
            getData: function(params) {
                return brandFctory.getAllBrands().then(function(data) {
                    var brands = data.result;

                    if ($scope.dataSearch) {
                        $scope.brands = $filter('filter')(brands, $scope.dataSearch);
                    } else {
                        $scope.brands = brands;
                    }

                    params.total($scope.brands.length);
                    return ($scope.brands.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                });
            }
        });

        $scope.refresh = function() {
            $scope.tableParams.reload();
        };

        $scope.addBrand = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/brands/brand.modal.html',
                controller: 'brandsModalCtrl',
                size: 'md',
                resolve: {
                    brandId: function() {
                        return null;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if(selectedItem){
                    $scope.refresh();
                }
            }, function() {});
        };

        $scope.updateBrand = function(item) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/brands/brand.modal.html',
                controller: 'brandsModalCtrl',
                size: 'md',
                resolve: {
                    brandId: function() {
                        return item.brandId;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if(selectedItem){
                    $scope.refresh();
                }
            }, function() {});
        };

        $scope.deleteSelected = function(item) {
            console.log('item: ', item);
            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                brandFctory.deleteBrandById(item.brandId).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.tableParams.reload();
                    }
                });
            }, function() {

            });
        };
    }
})();