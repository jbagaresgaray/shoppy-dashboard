(function() {
    'use strict';

    angular.module('starter')
        .config(brandConfig)
        .factory('brandFctory', brandFctory);

    brandConfig.$inject = ['$stateProvider'];

    function brandConfig($stateProvider) {
        $stateProvider
            .state('app.brands', {
                url: '/brands',
                templateUrl: 'modules/brands/brands.html',
                data: {
                    pageTitle: 'Brands'
                },
                controller: 'brandsCtrl'
            });

    }

    brandFctory.$inject = ['Restangular'];

    function brandFctory(Restangular) {
        return {
            getAllBrands: function() {
                return Restangular.all('brand').customGET().then(function(res) {
                    return res;
                }, function(err) {
                    return err.data;
                });
            },
            saveBrands: function(data) {
                var formData = new FormData();
                formData.append('b_file', data.b_file);
                formData.append('name', data.name);
                return Restangular.all('brand').withHttpConfig({
                    transformRequest: angular.identity
                }).customPOST(formData, '', undefined, {
                    'Content-Type': undefined
                }).then(function(res) {
                    return res;
                }, function(err) {
                    return err.data;
                });
            },
            getBrandById: function(Id) {
                return Restangular.all('brand').customGET(Id).then(function(res) {
                    return res;
                }, function(err) {
                    return err.data;
                });
            },
            updateBrandById: function(Id, data) {
                var formData = new FormData();
                formData.append('b_file', data.b_file);
                formData.append('name', data.name);
                return Restangular.all('brand/' + Id).withHttpConfig({
                    transformRequest: angular.identity
                }).customPUT(formData, '', undefined, {
                    'Content-Type': undefined
                }).then(function(res) {
                    return res;
                }, function(err) {
                    return err.data;
                });
            },
            deleteBrandById: function(Id) {
                return Restangular.all('brand').customDELETE(Id).then(function(res) {
                    return res;
                }, function(err) {
                    return err.data;
                });
            },
        };
    }
})();