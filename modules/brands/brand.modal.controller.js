(function() {
    'use strict';

    angular.module('starter')
        .controller('brandsModalCtrl', brandsModalCtrl);

    brandsModalCtrl.$inject = ['_', '$scope', '$uibModalInstance', 'brandFctory', 'brandId', 'toastr', '$timeout'];

    function brandsModalCtrl(_, $scope, $uibModalInstance, brandFctory, brandId, toastr, $timeout) {

        $scope.brand = {};
        $scope.withImage = false;
        $scope.isSaving = false;

        if (brandId) {
            brandFctory.getBrandById(brandId).then(function(data) {
                if (data && data.success) {
                    var brand = data.result;
                    if (!_.isEmpty(brand)) {
                        $scope.brand = brand;
                    }
                }
            });
        }

        $scope.upload = function(file) {
            if (file) {
                $scope.withImage = true;
            } else {
                $scope.withImage = false;
            }
        };

        $scope.save = function() {
            $scope.isSaving = true;
            if (brandId) {
                brandFctory.updateBrandById(brandId, $scope.brand).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.isSaving = false;
                        $timeout(function() {
                            $uibModalInstance.close('save');
                        }, 300);
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                        $scope.isSaving = false;
                    } else {
                        toastr.error(data.msg, 'Error');
                        $scope.isSaving = false;
                    }
                });
            } else {
                brandFctory.saveBrands($scope.brand).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.isSaving = false;
                        $timeout(function() {
                            $uibModalInstance.close('save');
                        }, 300);
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                        $scope.isSaving = false;
                    } else {
                        toastr.error(data.msg, 'Error');
                        $scope.isSaving = false;
                    }
                });
            }
        };

        $scope.removethumb = function() {
            $scope.brand.b_file = null;
            $scope.withImage = false;
        };


        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();