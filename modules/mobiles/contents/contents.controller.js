(function() {
	'use strict';

	angular.module('starter')
		.controller('mobileContentsCtrl', mobileContentsCtrl);

	mobileContentsCtrl.$inject = ['_', '$scope', '$state', 'contentsFctry','toastr'];

	function mobileContentsCtrl(_, $scope, $state, contentsFctry,toastr) {
		$scope.contents = [];

		contentsFctry.getAllMobileContents().then(function(data) {
			if (data && data.success) {
				var contents = data.result;
				if (!_.isEmpty(contents)){
					$scope.contents = contents;
				}
			} else if (!data.success && _.isArray(data.result)) {
				_.each(data.result, function(msg) {
					toastr.warning(msg.msg, 'Warning');
				});
			} else {
				toastr.error(data.msg, 'Error');
			}
		});

		$scope.composeNewsletter = function() {
			$state.go('mobile.composecontent');
		};
	}
})();