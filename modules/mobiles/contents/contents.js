(function() {
    'use strict';

    angular.module('starter')
        .config(config)
        .factory('contentsFctry', contentsFctry);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('mobile.contents', {
                url: '/contents',
                templateUrl: 'modules/mobiles/contents/contents.html',
                data: {
                    pageTitle: 'Content Management'
                },
                controller: 'mobileContentsCtrl'
            })
            .state('mobile.composecontent', {
                url: '/contents/compose',
                templateUrl: 'modules/mobiles/contents/compose.html',
                data: {
                    pageTitle: 'Compose Content'
                },
                controller: 'mobileContentsComposeCtrl'
            });
    }

    contentsFctry.$inject = ['Restangular'];

    function contentsFctry(Restangular) {
        return {
            getAllMobileContents: function() {
                return Restangular.all('mobile/cms').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            createMobileContent: function(data) {
                return Restangular.all('mobile/cms').customPOST(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getMobileContent: function(cmsId) {
                return Restangular.all('mobile/cms').customGET(cmsId).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            deleteMobileContent: function(cmsId) {
                return Restangular.all('mobile/cms').customDELETE(cmsId).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            updateMobileContent: function(cmsId, data) {
                return Restangular.all('mobile/cms/' + cmsId).customPUT(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            }
        };
    }
})();