(function() {
    'use strict';

    angular.module('starter')
        .controller('mobileContentsComposeCtrl', mobileContentsComposeCtrl);

    mobileContentsComposeCtrl.$inject = ['_', '$scope', '$state', 'contentsFctry', 'toastr', '$timeout'];

    function mobileContentsComposeCtrl(_, $scope, $state, contentsFctry, toastr, $timeout) {
        $scope.content = {};
        $scope.tags = [{
            text: 'just'
        }, {
            text: 'some'
        }, {
            text: 'cool'
        }, {
            text: 'tags'
        }];

        $scope.save = function() {
            contentsFctry.createMobileContent($scope.content).then(function(data) {
                if (data && data.success) {
                    toastr.success(data.msg, 'Success');
                    $timeout(function() {
                        $state.go('mobile.contents');
                    }, 300);
                } else if (!data.success && _.isArray(data.result)) {
                    _.each(data.result, function(msg) {
                        toastr.warning(msg.msg, 'Warning');
                    });
                } else {
                    toastr.error(data.msg, 'Error');
                }
            });
        };
    }
})();