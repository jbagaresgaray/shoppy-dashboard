(function() {
    'use strict';

    angular.module('starter')
        .config(config)
        .factory('bannersFctry', bannersFctry);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('mobile.banners', {
                url: '/banners',
                templateUrl: 'modules/mobiles/banners/banners.html',
                data: {
                    pageTitle: 'Mobile Banners'
                },
                controller: 'mobileBannersCtrl'
            });

    }

    bannersFctry.$inject = ['Restangular'];

    function bannersFctry(Restangular) {
        return {
            getAllBanners: function() {
                return Restangular.all('mobile/banner').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            createMobileBanner: function(data) {
                var formData = new FormData();
                formData.append('c_file', data.c_file);
                formData.append('name', data.name);
                formData.append('link', data.link);

                return Restangular.all('mobile/banner').withHttpConfig({
                    transformRequest: angular.identity
                }).customPOST(formData, '', undefined, {
                    'Content-Type': undefined
                }).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getMobileBanner: function(bannerId) {
                return Restangular.all('mobile/banner').customGET(bannerId).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            deleteMobileBanner: function(bannerId) {
                return Restangular.all('mobile/banner/' + bannerId).customDELETE().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            updateMobileBanner: function(bannerId, data) {
                var formData = new FormData();
                formData.append('c_file', data.c_file);
                formData.append('name', data.name);
                formData.append('link', data.link);
                return Restangular.all('mobile/banner/' + bannerId).withHttpConfig({
                    transformRequest: angular.identity
                }).customPUT(formData, '', undefined, {
                    'Content-Type': undefined
                }).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            publishBanner: function(bannerId) {
                return Restangular.all('mobile/banner/' + bannerId + '/publish').customPUT().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            unpublishBanner: function(bannerId) {
                return Restangular.all('mobile/banner/' + bannerId + '/unpublish').customPUT().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            }
        };
    }
})();