(function() {
    'use strict';

    angular.module('starter')
        .controller('mobileBannersCtrl', mobileBannersCtrl);

    mobileBannersCtrl.$inject = ['_', '$scope', '$state', '$uibModal', 'NgTableParams', 'ngDialog', 'toastr', '$filter', 'bannersFctry'];

    function mobileBannersCtrl(_, $scope, $state, $uibModal, NgTableParams, ngDialog, toastr, $filter, bannersFctry) {
        $scope.banners = [];
        $scope.dataSearch = '';

        $scope.tableParams = new NgTableParams({
            page: 1, // show first page
            count: 10, // count per page
        }, {
            getData: function(params) {
                return bannersFctry.getAllBanners().then(function(data) {
                    var category = data.result;

                    if ($scope.dataSearch) {
                        $scope.category = $filter('filter')(category, $scope.dataSearch);
                    } else {
                        $scope.category = category;
                    }

                    params.total($scope.category.length);
                    return ($scope.category.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                });
            }
        });

        $scope.refresh = function() {
            $scope.tableParams.reload();
        };

        $scope.addBanner = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/mobiles/banners/banners.modal.html',
                controller: 'mobileBannersModalCtrl',
                size: 'md',
                resolve: {
                    bannerId: function() {
                        return null;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.refresh();
                }
            }, function() {});
        };

        $scope.updateBanner = function(item) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/mobiles/banners/banners.modal.html',
                controller: 'mobileBannersModalCtrl',
                size: 'md',
                resolve: {
                    bannerId: function() {
                        return item.bannerId;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.refresh();
                }
            }, function() {});
        };

        $scope.deleteBanner = function(item) {
            console.log('item: ', item);
            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                bannersFctry.deleteMobileBanner(item.uuid).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.tableParams.reload();
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }, function() {

            });
        };

        $scope.publishBanner = function(action, item) {
            if (action === 'publish') {
                bannersFctry.publishBanner(item.uuid).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.tableParams.reload();
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else if (action === 'unpublish') {
                bannersFctry.unpublishBanner(item.uuid).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.tableParams.reload();
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
        };
    }
})();