(function() {
    'use strict';

    angular.module('starter')
        .controller('mobileBannersModalCtrl', mobileBannersModalCtrl);

    mobileBannersModalCtrl.$inject = ['_', '$scope', '$uibModalInstance', 'bannersFctry', 'contentsFctry', 'bannerId', 'toastr', '$timeout'];

    function mobileBannersModalCtrl(_, $scope, $uibModalInstance, bannersFctry, contentsFctry, bannerId, toastr, $timeout) {
        $scope.banner = {};
        $scope.contents = [];
        $scope.withImage = false;
        $scope.isSaving = false;

        if (bannerId) {
            bannersFctry.getMobileBanner(bannerId).then(function(data) {
                if (data && data.success) {
                    var banner = data.result;
                    if (!_.isEmpty(banner)) {
                        $scope.banner = banner;
                    }
                    console.log('$scope.banner: ',$scope.banner);
                }
            });
        }

        contentsFctry.getAllMobileContents().then(function(data) {
            if (data && data.success) {
                var contents = data.result;
                if (!_.isEmpty(contents)) {
                    $scope.contents = contents;
                }
                console.log('contents: ', $scope.contents);
            }
        });

        $scope.upload = function(file) {
            if (file) {
                $scope.withImage = true;
            } else {
                $scope.withImage = false;
            }
        };

        $scope.save = function() {
            $scope.isSaving = true;
            if (bannerId) {
                bannersFctry.updateMobileBanner(bannerId, $scope.banner).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.isSaving = false;
                        $timeout(function() {
                            $uibModalInstance.close('save');
                        }, 300);
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                        $scope.isSaving = false;
                    } else {
                        toastr.error(data.msg, 'Error');
                        $scope.isSaving = false;
                    }
                });
            } else {
                bannersFctry.createMobileBanner($scope.banner).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.isSaving = false;
                        $timeout(function() {
                            $uibModalInstance.close('save');
                        }, 300);
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                        $scope.isSaving = false;
                    } else {
                        toastr.error(data.msg, 'Error');
                        $scope.isSaving = false;
                    }
                });
            }
        };

        $scope.removethumb = function() {
            $scope.banner.c_file = null;
            $scope.withImage = false;
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();