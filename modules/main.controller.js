(function() {
    'use strict';

    angular.module('starter')
        .controller('mainContCtrl', mainContCtrl);

    mainContCtrl.$inject = ['async', '$scope', '$rootScope', '$state', 'auth', 'localStorageService', '$location', 'sellersFctry'];

    function mainContCtrl(async, $scope, $rootScope, $state, auth, localStorageService, $location, sellersFctry) {
        $scope.currentUser = {};
        $scope.sellerInfo = {};

        $scope.refreshSellers = function() {
            async.parallel([
                function(callback) {
                    sellersFctry.getAllSellers().then(function(data) {
                        if (data && data.success) {
                            $scope.sellerInfo.sellers = data.result.length || 0;
                        } else {
                            $scope.sellerInfo.sellers = 0;
                        }
                        callback();
                    });
                },
                function(callback) {
                    sellersFctry.getAllApplicants().then(function(data) {
                        if (data && data.success) {
                            $scope.sellerInfo.applicants = data.result.length || 0;
                        } else {
                            $scope.sellerInfo.applicants = 0;
                        }
                        callback();
                    });
                }
            ]);
        };


        if (auth.getUser()) {
            $scope.currentUser = auth.getUser().user;
            $scope.refreshSellers();
        } else {
            $location.path('/login');
        }


        $scope.logout = function() {
            console.log('logout');
            auth.logout().then(function(data) {
                if (data.success) {
                    localStorageService.remove('user');
                    localStorageService.remove('authdata');

                    $location.path('/login');
                }
            });
        };
    }
})();