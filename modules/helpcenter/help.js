(function() {
    'use strict';

    angular.module('starter')
        .config(config)
        .factory('helpFctry', helpFctry);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('help.issuetracker', {
                url: '/issuetracker',
                templateUrl: 'modules/helpcenter/issuetracker.html',
                data: {
                    pageTitle: 'Issue Tracker'
                }
            })
            .state('help.helpcentre', {
                url: '/helpcenter',
                templateUrl: 'modules/helpcenter/inbox.html',
                controller: 'helpCentreCtrl',
                data: {
                    pageTitle: 'Help Centre'
                }

            })
            .state('help.composer', {
                url: '/composer',
                templateUrl: 'modules/helpcenter/compose.html',
                controller: 'helpCentreComposeCtrl',
                data: {
                    pageTitle: 'Compose'
                }
            })
            .state('help.preview', {
                url: '/preview/:postId',
                templateUrl: 'modules/helpcenter/preview.html',
                controller: 'helpCentreComposeCtrl',
                data: {
                    pageTitle: 'Preview'
                }
            });
    }

    helpFctry.$inject = ['Restangular'];

    function helpFctry(Restangular) {
        return {
            getAllCategory: function() {
                return Restangular.all('helpcentre/category').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            createHelpPost: function(data) {
                return Restangular.all('helpcentre').customPOST(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getAllHelpPosts: function() {
                return Restangular.all('helpcentre').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getPostByPostId: function(postId) {
                return Restangular.all('helpcentre').customGET(postId).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            }
        };
    }
})();