(function() {
    'use strict';

    angular.module('starter')
        .controller('helpCentreCtrl', helpCentreCtrl)
        .controller('helpCentreComposeCtrl', helpCentreComposeCtrl);

    helpCentreCtrl.$inject = ['_','$scope', 'helpFctry'];

    function helpCentreCtrl(_,$scope, helpFctry) {
        $scope.categories = [];
        $scope.posts = [];

        $scope.loadHelpCategory = function() {
            helpFctry.getAllCategory().then(function(data) {
                console.log('categories: ', data);
                if (data && data.success) {
                    $scope.categories = data.result;
                }
            }, function(error) {
                console.log('error: ', error);
            });
        };

        $scope.loadPosts = function() {
            helpFctry.getAllHelpPosts().then(function(data) {
                if (data && data.success) {
                    $scope.posts = data.result;
                    _.each($scope.posts, function(row) {
                        row.selected = false;
                        if (row.post_created) {
                            row.post_created = new Date(row.post_created);
                        }
                    });
                    console.log('posts: ', $scope.posts);
                }
            }, function(error) {
                console.log('error: ', error);
            });
        };

        $scope.loadHelpCategory();
        $scope.loadPosts();

    }


    helpCentreComposeCtrl.$inject = ['$scope','$stateParams', 'helpFctry', 'toastr', '$timeout', '_'];

    function helpCentreComposeCtrl($scope,$stateParams, helpFctry, toastr, $timeout, _) {
        $scope.categories = [];
        $scope.helpObj = {};
        $scope.isSaving = false;

        $scope.loadHelpCategory = function() {
            helpFctry.getAllCategory().then(function(data) {
                console.log('categories: ', data);
                if (data && data.success) {
                    $scope.categories = data.result;
                }
            }, function(error) {
                console.log('error: ', error);
            });
        };

        $scope.savePost = function() {
            helpFctry.createHelpPost($scope.helpObj).then(function(data) {
                if (data && data.success) {
                    toastr.success(data.msg, 'Success');
                    $scope.isSaving = false;
                } else if (!data.success && _.isArray(data.result)) {
                    _.each(data.result, function(msg) {
                        toastr.warning(msg.msg, 'Warning');
                    });
                    $scope.isSaving = false;
                } else {
                    toastr.error(data.msg, 'Error');
                    $scope.isSaving = false;
                }
            }, function(error) {
                console.log('error: ', error);
            });
        };

        $scope.loadPostDetails = function(postId){
            helpFctry.getPostByPostId(postId).then(function(data){
                if (data && data.success) {
                    $scope.helpObj = data.result;

                    if($scope.helpObj.post_created){
                        $scope.helpObj.post_created = new Date($scope.helpObj.post_created);
                    }
                    console.log('$scope.helpObj: ',$scope.helpObj);
                } else {
                    toastr.error(data.msg, 'Error');
                    $scope.isSaving = false;
                }
            },function(error){
                console.log('error: ',error);
            });
        };

        if (_.isUndefined($stateParams.postId)){
            $scope.loadHelpCategory();
        }else{
            $scope.loadPostDetails($stateParams.postId);
        }

    }

})();