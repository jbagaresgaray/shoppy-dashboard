(function() {
    'use strict';

    angular.module('starter')
        .controller('categoryModalCtrl', categoryModalCtrl);

    categoryModalCtrl.$inject = ['_', '$scope', '$uibModalInstance', 'categoryFctry', 'categoryId', 'toastr', '$timeout'];

    function categoryModalCtrl(_, $scope, $uibModalInstance, categoryFctry, categoryId, toastr, $timeout) {
        $scope.category = {};
        $scope.withImage = false;
        $scope.isSaving = false;

        if (categoryId) {
            categoryFctry.getCategoryById(categoryId).then(function(data) {
                if (data && data.success) {
                    var category = data.result;
                    if (!_.isEmpty(category)) {
                        $scope.category = category;
                    }
                }
            });
        }

        $scope.upload = function(file) {
            if (file) {
                $scope.withImage = true;
            } else {
                $scope.withImage = false;
            }
        };

        $scope.save = function() {
            $scope.isSaving = true;
            if (categoryId) {
                categoryFctry.updateCategoryById(categoryId, $scope.category).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.isSaving = false;
                        $timeout(function() {
                            $uibModalInstance.close('save');
                        }, 300);
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                        $scope.isSaving = false;
                    } else {
                        toastr.error(data.msg, 'Error');
                        $scope.isSaving = false;
                    }
                });
            } else {

                categoryFctry.saveCategories($scope.category).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.isSaving = false;
                        $timeout(function() {
                            $uibModalInstance.close('save');
                        }, 300);
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                        $scope.isSaving = false;
                    } else {
                        toastr.error(data.msg, 'Error');
                        $scope.isSaving = false;
                    }
                });
            }
        };

        $scope.removethumb = function() {
            $scope.category.c_file = null;
            $scope.withImage = false;
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();