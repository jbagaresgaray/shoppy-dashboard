(function() {
    'use strict';

    angular.module('starter')
        .config(categoryConfig)
        .factory('categoryFctry', categoryFctry)
        .factory('subCategoryFctry', subCategoryFctry);

    categoryConfig.$inject = ['$stateProvider'];

    function categoryConfig($stateProvider) {
        $stateProvider
            .state('app.category', {
                url: '/category',
                templateUrl: 'modules/category/category.html',
                data: {
                    pageTitle: 'Category'
                },
                controller: 'categoryCtrl',
            })
            .state('app.subcategory', {
                url: '/subcategory/{categoryId}',
                templateUrl: 'modules/category/subcategory/subcategory.html',
                data: {
                    pageTitle: 'Sub-Category'
                },
                controller: 'subCategoryCtrl',
            });

    }

    categoryFctry.$inject = ['Restangular'];

    function categoryFctry(Restangular) {
        return {
            getAllCategories: function() {
                return Restangular.all('category').customGET().then(function(res) {
                    return res;
                }, function(err) {
                    return err.data;
                });
            },
            saveCategories: function(data) {
                var formData = new FormData();
                formData.append('c_file', data.c_file);
                formData.append('name', data.name);
                return Restangular.all('category').withHttpConfig({
                    transformRequest: angular.identity
                }).customPOST(formData, '', undefined, {
                    'Content-Type': undefined
                }).then(function(res) {
                    return res;
                }, function(err) {
                    return err.data;
                });
            },
            getCategoryById: function(Id) {
                return Restangular.all('category').customGET(Id).then(function(res) {
                    return res;
                }, function(err) {
                    return err.data;
                });
            },
            updateCategoryById: function(Id, data) {
                var formData = new FormData();
                formData.append('c_file', data.c_file);
                formData.append('name', data.name);
                return Restangular.all('category/' + Id).withHttpConfig({
                    transformRequest: angular.identity
                }).customPUT(formData, '', undefined, {
                    'Content-Type': undefined
                }).then(function(res) {
                    return res;
                }, function(err) {
                    return err.data;
                });
            },
            deleteCategoryById: function(Id) {
                return Restangular.all('category').customDELETE(Id).then(function(res) {
                    return res;
                }, function(err) {
                    return err.data;
                });
            },
        };
    }

    subCategoryFctry.$inject = ['Restangular'];

    function subCategoryFctry(Restangular) {
        return {
            getAllSubCategories: function(categoryId) {
                return Restangular.all('category/' + categoryId + '/subcategory').customGET().then(function(res) {
                    return res;
                }, function(err) {
                    return err.data;
                });
            },
            saveSubCategories: function(categoryId, data) {
                var formData = new FormData();
                formData.append('c_file', data.c_file);
                formData.append('name', data.name);
                return Restangular.all('category/' + categoryId + '/subcategory').withHttpConfig({
                    transformRequest: angular.identity
                }).customPOST(formData, '', undefined, {
                    'Content-Type': undefined
                }).then(function(res) {
                    return res;
                }, function(err) {
                    return err.data;
                });
            },
            getSubCategoryById: function(categoryId, Id) {
                return Restangular.all('category/' + categoryId + '/subcategory/' + Id).customGET().then(function(res) {
                    return res;
                }, function(err) {
                    return err.data;
                });
            },
            updateSubCategoryById: function(categoryId, Id, data) {
                var formData = new FormData();
                formData.append('c_file', data.c_file);
                formData.append('name', data.name);
                return Restangular.all('category/' + categoryId + '/subcategory/' + Id).withHttpConfig({
                    transformRequest: angular.identity
                }).customPUT(formData, '', undefined, {
                    'Content-Type': undefined
                }).then(function(res) {
                    return res;
                }, function(err) {
                    return err.data;
                });
            },
            deleteSubCategoryById: function(categoryId, Id) {
                return Restangular.all('category/' + categoryId + '/subcategory/' + Id).customDELETE().then(function(res) {
                    return res;
                }, function(err) {
                    return err.data;
                });
            },
        };
    }
})();