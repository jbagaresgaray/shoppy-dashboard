(function() {
    'use strict';

    angular.module('starter')
        .controller('categoryCtrl', categoryCtrl);

    categoryCtrl.$inject = ['$scope', '$state', '$uibModal', 'NgTableParams', 'categoryFctry', 'ngDialog', 'toastr', '$filter'];

    function categoryCtrl($scope, $state, $uibModal, NgTableParams, categoryFctry, ngDialog, toastr, $filter) {
        $scope.category = [];
        $scope.dataSearch = '';

        $scope.tableParams = new NgTableParams({
            page: 1, // show first page
            count: 10, // count per page
        }, {
            getData: function(params) {
                return categoryFctry.getAllCategories().then(function(data) {
                    var category = data.result;

                    if ($scope.dataSearch) {
                        $scope.category = $filter('filter')(category, $scope.dataSearch);
                    } else {
                        $scope.category = category;
                    }

                    params.total($scope.category.length);
                    return ($scope.category.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                });
            }
        });

        $scope.refresh = function() {
            $scope.tableParams.reload();
        };

        $scope.addCategory = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/category/category.modal.html',
                controller: 'categoryModalCtrl',
                size: 'md',
                resolve: {
                    categoryId: function() {
                        return null;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if(selectedItem){
                    $scope.refresh();
                }
            }, function() {});
        };

        $scope.updateCategory = function(item) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/category/category.modal.html',
                controller: 'categoryModalCtrl',
                size: 'md',
                resolve: {
                    categoryId: function() {
                        return item.categoryId;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if(selectedItem){
                    $scope.refresh();
                }
            }, function() {});
        };

        $scope.addSubCategory = function(item) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/category/subcategory/subcategory.modal.html',
                controller: 'subCategoryModalCtrl',
                size: 'md',
                resolve: {
                    categoryId: function() {
                        return item.categoryId;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if(selectedItem){
                    $scope.refresh();
                }
            }, function() {});
        };

        $scope.viewSubCategory = function(item) {
            $state.go('app.subcategory', {
                categoryId: item.categoryId
            });
        };

        $scope.deleteSelected = function(item) {
            console.log('item: ', item);
            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                categoryFctry.deleteCategoryById(item.categoryId).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.tableParams.reload();
                    }
                });
            }, function() {

            });
        };
    }
})();