(function() {
    'use strict';

    angular.module('starter')
        .controller('subCategoryModalCtrl', subCategoryModalCtrl);

    subCategoryModalCtrl.$inject = ['_', '$scope', '$stateParams', '$uibModalInstance', 'subCategoryFctry', 'categoryFctry', 'subCategoryId', 'toastr', '$timeout'];

    function subCategoryModalCtrl(_, $scope, $stateParams, $uibModalInstance, subCategoryFctry, categoryFctry, subCategoryId, toastr, $timeout) {
        $scope.subcategory = {};
        $scope.category = {};
        $scope.withImage = false;
        $scope.isSaving = false;

        if ($stateParams.categoryId) {
            categoryFctry.getCategoryById($stateParams.categoryId).then(function(data) {
                if (data && data.success) {
                    var category = data.result;
                    if (!_.isEmpty(category)) {
                        $scope.category = category;
                    }
                }
            });

            if (subCategoryId) {
                subCategoryFctry.getSubCategoryById($stateParams.categoryId, subCategoryId).then(function(data) {
                    if (data && data.success) {
                        var subcategory = data.result;
                        if (!_.isEmpty(subcategory)) {
                            $scope.subcategory = subcategory;
                        }
                    }
                });
            }
        }

        $scope.upload = function(file) {
            if (file) {
                $scope.withImage = true;
            } else {
                $scope.withImage = false;
            }
        };

        $scope.save = function() {
            $scope.isSaving = true;
            $scope.subcategory.categoryId = $scope.category.categoryId;
            if (subCategoryId) {
                subCategoryFctry.updateSubCategoryById($stateParams.categoryId, subCategoryId, $scope.subcategory).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.isSaving = false;
                        $timeout(function() {
                            $uibModalInstance.close('save');
                        }, 300);
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                        $scope.isSaving = false;
                    } else {
                        toastr.error(data.msg, 'Error');
                        $scope.isSaving = false;
                    }
                });
            } else {
                subCategoryFctry.saveSubCategories($stateParams.categoryId, $scope.subcategory).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.isSaving = false;
                        $timeout(function() {
                            $uibModalInstance.close('save');
                        }, 300);
                    } else if (!data.success && _.isArray(data.result)) {
                        _.each(data.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                        $scope.isSaving = false;
                    } else {
                        toastr.error(data.msg, 'Error');
                        $scope.isSaving = false;
                    }
                });
            }
        };

        $scope.removethumb = function() {
            $scope.category.c_file = null;
            $scope.withImage = false;
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();