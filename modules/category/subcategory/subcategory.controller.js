(function() {
    'use strict';

    angular.module('starter')
        .controller('subCategoryCtrl', subCategoryCtrl);

    subCategoryCtrl.$inject = ['$scope', '$uibModal', '$state', '$stateParams', 'subCategoryFctry', 'ngDialog', 'toastr', '$filter', 'NgTableParams'];

    function subCategoryCtrl($scope, $uibModal, $state, $stateParams, subCategoryFctry, ngDialog, toastr, $filter, NgTableParams) {
        $scope.subcategory = [];
        $scope.dataSearch = '';

        $scope.tableParams = new NgTableParams({
            page: 1, // show first page
            count: 10, // count per page
        }, {
            getData: function(params) {
                return subCategoryFctry.getAllSubCategories($stateParams.categoryId).then(function(data) {
                    var subcategory = data.result;

                    if ($scope.dataSearch) {
                        $scope.subcategory = $filter('filter')(subcategory, $scope.dataSearch);
                    } else {
                        $scope.subcategory = subcategory;
                    }

                    params.total($scope.subcategory.length);
                    return ($scope.subcategory.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                });
            }
        });

        $scope.goBack = function() {
            $state.go('app.category');
        };

        $scope.refresh = function() {
            $scope.tableParams.reload();
        };

        $scope.addSubCategory = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/category/subcategory/subcategory.modal.html',
                controller: 'subCategoryModalCtrl',
                size: 'md',
                resolve: {
                    subCategoryId: function() {
                        return null;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.refresh();
                }
            }, function() {});
        };

        $scope.updateSubCategory = function(item) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/category/subcategory/subcategory.modal.html',
                controller: 'subCategoryModalCtrl',
                size: 'md',
                resolve: {
                    subCategoryId: function() {
                        return item.subcategoryId;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.refresh();
                }
            }, function() {});
        };

        $scope.deleteSelected = function(item) {
            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                subCategoryFctry.deleteSubCategoryById($stateParams.categoryId, item.subcategoryId).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'Success');
                        $scope.tableParams.reload();
                    }
                });
            }, function() {

            });
        };
    }
})();