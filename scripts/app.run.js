(function() {
    'use strict';

    angular.module('starter')
        .run(['$rootScope', '$state', 'localStorageService', 'jwtHelper',
            function($rootScope, $state, localStorageService, jwtHelper) {
                $rootScope.$state = $state;

                $rootScope.$on('unauthorized', function() {
                    localStorageService.remove('user');
                    localStorageService.remove('authdata');

                    $state.go('login');
                });

                if (localStorageService.get('user')) {
                    var storagedToken = localStorageService.get('user').token;
                    if (jwtHelper.isTokenExpired(storagedToken)) {
                        localStorageService.remove('user');
                        localStorageService.remove('authdata');

                        $state.go('login');
                    }
                }
            }
        ])
        /*.factory('socket', ['$rootScope', 'API_URL', 'API_URL_PROD', 'API_VERSION', 'isPRODUCTION', function($rootScope, API_URL, API_URL_PROD, API_VERSION, isPRODUCTION) {
            var baseUrl = (isPRODUCTION === true ? API_URL_PROD : API_URL);
            var socket = window.io.connect(baseUrl);
            return {
                on: function(eventName, callback) {
                    socket.on(eventName, function() {
                        var args = arguments;
                        $rootScope.$apply(function() {
                            callback.apply(socket, args);
                        });
                    });
                },
                emit: function(eventName, data, callback) {
                    socket.emit(eventName, data, function() {
                        var args = arguments;
                        $rootScope.$apply(function() {
                            if (callback) {
                                callback.apply(socket, args);
                            }
                        });
                    });
                }
            };
        }])*/
        .filter('propsFilter', function() {
            return function(items, props) {
                console.log('propsFilter');
                var out = [];

                if (angular.isArray(items)) {
                    var keys = Object.keys(props);

                    items.forEach(function(item) {
                        var itemMatches = false;

                        for (var i = 0; i < keys.length; i++) {
                            var prop = keys[i];
                            var text = props[prop].toLowerCase();
                            if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                                itemMatches = true;
                                break;
                            }
                        }

                        if (itemMatches) {
                            out.push(item);
                        }
                    });
                } else {
                    // Let the output be the input untouched
                    out = items;
                }

                return out;
            };
        });

})();