(function() {
    'use strict';

    angular.module('starter')
        .factory('authInterceptor', authInterceptor);

    authInterceptor.$inject = ['$rootScope', '$q', '$location', 'localStorageService','jwtHelper'];

    function authInterceptor($rootScope, $q, $location, localStorageService,jwtHelper) {
        return {
            request: function(config) {
                config.headers = config.headers || {};

                if (localStorageService.get('user')) {
                    var token = localStorageService.get('user').token;
                    config.headers.Authorization = 'Bearer ' + token;
                }
                // $activityIndicator.startAnimating();
                return config || $q.when(config);
            },
            response: function(response) {
                // $activityIndicator.stopAnimating();
                // return response || $q.when(response);

                if (localStorageService.get('user')) {
                    var storagedToken = localStorageService.get('user').token;
                    if (jwtHelper.isTokenExpired(storagedToken)) {
                        localStorageService.remove('token');
                        localStorageService.remove('user');

                        $location.path('/login');
                    }
                }
                return response || $q.when(response);
            },
            responseError: function(response) {
                if (response.status === 401) {
                    $rootScope.$broadcast('unauthorized');
                    $location.path('/login');
                } else if (response.status === 403) {
                    $rootScope.$broadcast('unauthorized');
                    $location.path('/login');
                }
                // $activityIndicator.stopAnimating();
                return $q.reject(response);
            }
        };
    }

})();