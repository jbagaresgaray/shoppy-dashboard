(function() {
    'use strict';

    angular.module('starter', [
            'ngSanitize',
            'ui.router',
            'ui.router.state.events',
            'ui.bootstrap',
            'ui.checkbox',
            'restangular',
            'angularMoment',
            'ngIdle',
            'LocalStorageModule',
            'toastr',
            'ngTable',
            'ngDialog',
            'ngFileUpload',
            'angular-jwt',
            'angular-peity',
            'angular-flot',
            'angles',
            'summernote',
            'ngTagsInput',
            'ui.select',
            'ui.sortable'
        ])
        .constant('API_URL', 'http://localhost:3000')
        .constant('API_URL_PROD', 'https://shoppy-123456789.herokuapp.com')
        .constant('API_VERSION', '/app/1/')
        .constant('isPRODUCTION', false)
        .constant('_', window._)
        .constant('moment', window.moment)
        .constant('async', window.async)
        .constant('EVAL_CATEGORY',{
            applicants: 'APPLICANTS',
            inReview: 'IN-REVIEW',
            forApproval: 'FOR-APPROVAL',
            completed: 'COMPLETED'
        })
        .constant('EVAL_CATEGORY_LIST',[{
            key: 'applicants',
            value: 'APPLICANTS'
        },{
            key: 'inReview',
            value: 'IN-REVIEW'
        },{
            key: 'forApproval',
            value: 'FOR-APPROVAL'
        },{
            key: 'completed',
            value: 'COMPLETED'
        }])
        .constant('EVAL_STATUS', {
            success: 'success',
            warning: 'warning',
            danger: 'danger',
            info: 'info'
        })
        .constant('EVAL_STATUS_LIST',[{
            key: 'success',
            value: 'success'
        },{
            key: 'warning',
            value: 'warning'
        },{
            key: 'danger',
            value: 'danger'
        },{
            key: 'info',
            value: 'info'
        }]);
})();
