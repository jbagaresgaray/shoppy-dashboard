(function() {
    'use strict';

    angular.module('starter')
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', 'IdleProvider', 'KeepaliveProvider', 'RestangularProvider', 'API_URL', 'API_URL_PROD', 'API_VERSION', 'isPRODUCTION', 'localStorageServiceProvider'];

    function config($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, IdleProvider, KeepaliveProvider, RestangularProvider, API_URL, API_URL_PROD, API_VERSION, isPRODUCTION, localStorageServiceProvider) {

        $locationProvider.html5Mode({
            enabled: false,
            requireBase: false
        });
        $locationProvider.hashPrefix('');

        // Configure Idle settings
        IdleProvider.idle(5); // in seconds
        IdleProvider.timeout(120); // in seconds

        $urlRouterProvider.otherwise('/login');

        localStorageServiceProvider.setPrefix('shoppy');
        var baseUrl = (isPRODUCTION === true ? API_URL_PROD : API_URL) + API_VERSION;
        RestangularProvider.setBaseUrl(baseUrl);
        $httpProvider.interceptors.push('authInterceptor');

        $stateProvider
            .state('dashboards', {
                abstract: true,
                url: '/dashboards',
                templateUrl: 'modules/common/content.html',
                controller: 'mainContCtrl'
            })
            .state('app', {
                abstract: true,
                url: '/app',
                templateUrl: 'modules/common/content.html',
                controller: 'mainContCtrl'
            })
            .state('mobile', {
                abstract: true,
                url: '/mobile',
                templateUrl: 'modules/common/content.html',
                controller: 'mainContCtrl'
            })
            .state('help', {
                abstract: true,
                url: '/help',
                templateUrl: 'modules/common/content.html',
                controller: 'mainContCtrl'
            })
            .state('dashboards.dashboard_1', {
                url: '/dashboard_1',
                templateUrl: 'modules/dashboard_1.html',
            })
            .state('dashboards.dashboard_2', {
                url: '/dashboard_2',
                templateUrl: 'modules/dashboard_2.html',
                data: {
                    pageTitle: 'Dashboard 2'
                }
            })
            .state('dashboards.dashboard_3', {
                url: '/dashboard_3',
                templateUrl: 'modules/dashboard_3.html',
                data: {
                    pageTitle: 'Dashboard 3'
                },
            })
            .state('dashboards_top', {
                abstract: true,
                url: '/dashboards_top',
                templateUrl: 'modules/common/content_top_navigation.html',
            })
            .state('dashboards_top.dashboard_4', {
                url: '/dashboard_4',
                templateUrl: 'modules/dashboard_4.html',
                data: {
                    pageTitle: 'Dashboard 4'
                },
            })
            .state('dashboards.dashboard_4_1', {
                url: '/dashboard_4_1',
                templateUrl: 'modules/dashboard_4_1.html',
                data: {
                    pageTitle: 'Dashboard 4'
                }
            })
            .state('dashboards.dashboard_5', {
                url: '/dashboard_5',
                templateUrl: 'modules/dashboard_5.html',
                data: {
                    pageTitle: 'Dashboard 5'
                }
            })
            .state('layouts', {
                url: '/layouts',
                templateUrl: 'modules/layouts.html',
                data: {
                    pageTitle: 'Layouts'
                },
            });
    }
})();